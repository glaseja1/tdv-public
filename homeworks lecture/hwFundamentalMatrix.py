import numpy as np
import random

# the F matrix must be skew symmetric
max_ = 1000;
f11 = 0;
f12 = 0
f13 = 0;
f21 = 0
f22 = 0;
f23 = 0;
f31 = 0;
f32 = 0;
f33 = 0;

e23 = 0
e13 = 0

def setF1():
    f13 = random.randint(1, max_)
    f31 = -f13
def setF2():
    f12 = random.randint(1, max_)
    f21 = -f12;
def setF3():
    f23 = random.randint(1, max_)
    f32 = -f23;

setF2();

a = random.randint(1, max_);
b = random.randint(1, max_);
c = random.randint(1, max_);

# f must be skew symetric
F = np.array([[f11, f12, f13], [f21, f22, f23], [f31, f32, f33]]);
print('F: {}'.format(F))
P1 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]]);
e2x = np.array([[0, -e23, 0], [e23, 0, 0], [0, 0, 0]]);
e2 = np.array([[0], [0], [e23]]);
e1T = [[0, 0, e13]];
X = [[a], [b], [c], [0]];
xprojected1 = P1@X;
P2 = e2x@F + e2@e1T;

print(P2)
# add column
P2 = np.hstack( ( P2, e2 ) )
print(P2)

#P2 = [[-f21, -f22, -f23, Part[e2, 1, 1]], [f11, f12, f13,
#   Part[e2, 2, 1]], [0, 0, e13 e23, Part[e2, 3, 1]]]
xprojected2 = P2@X;

print(xprojected1)
print(xprojected2)
#epipolar constraint must apply
res = xprojected1.reshape(1, 3)@F@xprojected2.reshape(3, 1);
print(res[0][0])
if(res[0][0] == 0):
    print('epipolar constraint ok')
else:
    print('epipolar constraint NOT ok')
