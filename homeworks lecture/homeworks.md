# homeworks
The results of the homeworks (pointwise) shall be visible in the [BRUTE](https://cw.felk.cvut.cz/brute/student/) web application

For creating pdf from images, use [smallpdf.com](smallpdf.com), using tor

## 1] optional - Lecture 2, slide 5 (1 point), SUBMITTED
<details>
  <summary>Click to expand!</summary>

[lecture 2 pdf](../pdf/lecture2.pdf)

For euclidean mapping.
The mapping preserves lengths:

Let
```math
x_i' = Hx_i',
(x_i)_3 = 1
```

```math
||\underline{x_2}' - \underline{x_1}'|| = ||H(\underline{x_2}) - H(\underline{x_1})|| = ||H(\underline{x_2} - \underline{x_1})|| = ? = ||\underline{x_2} - \underline{x_1}||
```

#### Solution?
Euclidean norm can be understood as the length of a vector
```math
||x|| = \sqrt{x_1^2 + ... x_n^2}
```
Well, and as we know that homography does not really work with a scale, it does not matter.

#### consultation

For euclidean mapping.
The mapping preserves lengths:

Let
```math
x_i' = Hx_i',
(x_i)_3 = 1
```

```math
||\underline{x_2}' - \underline{x_1}'|| = ||H(\underline{x_2}) - H(\underline{x_1})||
= ||H(\underline{x_2} - \underline{x_1})||
= ?
= ||\underline{x_2} - \underline{x_1}||
```

```math
||H(\underline{x_2} - \underline{x_1})|| =
||\begin{bmatrix}
R & t \\
0 & 1
\end{bmatrix} *
\begin{bmatrix}
x_2 - x_1 \\
1
\end{bmatrix}||
=
||\begin{bmatrix}
R * (x_2 - x_1) + t & 1
\end{bmatrix}||
=
\sqrt{(R * (x_2 - x_1) + t)^2 + 1^2}
```
```math
||x|| = \sqrt{x_1^2 + ... x_n^2}
```

#### Advice from student
roznasobit s H jako matice RT, vznikne pak $`R*R^-1`$, coz se vyrusi

</details>

## 2] Lecture 2, slide 12 (1 point), SUBMITTED
[lecture 2 pdf](../pdf/lecture2.pdf)

Feedback
>  Dal jste si s tim hodne prace. Ja bych navrhl rychlejsi a bezpecnejsi postup: Mejme jednotkove bazove vektory e_u, e_v, e'_u, e'_v, kde ty carkovane sviraji uhel theta, takze cos(theta)=e'_u*e'_v, kde * je skalarni soucin a ty necarkovane jsou ortogonalni, takze e_u*e_v = 0. Zavedeme skalarni koeficienty (souradnice) u, v, u', v', Mejme nejaky bod x (vektor) v rovine. Ten vyjadrime jako linearni kombinaci bazovych vektoru tak jak to je napsano na slajdu se zadanim. To je vektorova rovnice, ze ktere potrebujeme eliminovat e_u, e_v, e'_u, e'_v. To muzeme udelat napriklad tak, ze levou stranu rovnice skalarne nasobime nejdriv e_u a potom znovu e_v, po cemz vyjadrime skalrni souciny, ktere budou cos(theta), sin(theta) a nula. Dostaneme tak dve skalarni rovnice, ktere vyresime. Vysledek prepiseme do maticove formy a je to. Je na to potreba cca 10minut

<details>
  <summary>Click to expand!</summary>

Decompose K to simple maps and give f, a, θ, u0, v0 a precise meaning

We find matrices for maps:
#### scale
from [wiki](https://en.wikipedia.org/wiki/Scaling_(geometry))

```math
\begin{bmatrix}
v_x & 0 & 0 & 0 \\
0 & v_y & 0 & 0 \\
0 & 0 & v_z & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
```

For purpose of 2D, and arguments **af**, **f** required:
```math
\begin{bmatrix}
af & 0 & 0 \\
0 & f & 0 \\
0 & 0 & 1  
\end{bmatrix}
```

#### skew
from [wiki (shear matrix)](https://en.wikipedia.org/wiki/Shear_matrix)
```math
\begin{bmatrix}
1 & 0 & 0 & lambda & 0 \\
0 & 1 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 & 0 \\
0 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 & 0
\end{bmatrix}
```

For purpose of 2D, and arguments **lambda** required:
```math
\begin{bmatrix}
1 & 0 & lambda \\
0 & 1 & 0 \\
0 & 0 & 1  
\end{bmatrix}
```

Shear matrix:
```math
\begin{bmatrix}
1 & \frac{s}{f_x} & 0 \\
0 & 1 & 0 \\
0 & 0 & 1  
\end{bmatrix}
```

#### translate
from [wiki](https://en.wikipedia.org/wiki/Translation_(geometry))
```math
\begin{bmatrix}
1 & 0 & 0 & v_x  \\
0 & 1 & 0 & v_y  \\
0 & 0 & 1 & v_z  \\
0 & 0 & 0 & 1  
\end{bmatrix}
```

For purpose of 2D, and arguments **tx, ty** required:
```math
\begin{bmatrix}
1 & 0 & t_x \\
0 & 1 & t_y \\
0 & 0 & 1  
\end{bmatrix}
```

#### putting it all together
We are supposed to map by **scale**, then by **skew**, then by **translation**.

```math
K=T*SKEW*SCALE=
\begin{bmatrix}
1 & 0 & t_x \\
0 & 1 & t_y \\
0 & 0 & 1  
\end{bmatrix}
*
\begin{bmatrix}
1 & \frac{s}{f_x} & 0 \\
0 & 1 & 0 \\
0 & 0 & 1  
\end{bmatrix}
*
\begin{bmatrix}
af & 0 & 0 \\
0 & f & 0 \\
0 & 0 & 1  
\end{bmatrix}
```
K is expected to be
```math
K = \begin{bmatrix}
af & -afcot(\Phi) & u_0 \\
0 & \frac{f}{sin(\Phi)} & u_0 \\
0 & 0 & 1  
\end{bmatrix}
```

>  This is not yet finished

#### Advice from teacher
 - find matrix for skew and other ones, they can be googled
 - take v0, v0' and the angle between them is the cot in the matrix, that is how you get the cot and sin...

#### Advice from student
he decomposed it as follows:
```math
K=T*SKEW (Kw) *SCALE=
\begin{bmatrix}
1 & 0 & t_x \\
0 & 1 & t_y \\
0 & 0 & 1  
\end{bmatrix}
*
\begin{bmatrix}
a & -a * cot(\Phi) & 0 \\
0 & \frac{1}{sin(\Phi)} & 0 \\
0 & 0 & 1  
\end{bmatrix}
*
\begin{bmatrix}
f & 0 & 0 \\
0 & f & 0 \\
0 & 0 & 1  
\end{bmatrix}
```
K is expected to be
```math
K = \begin{bmatrix}
af & -afcot(\Phi) & u_0 \\
0 & \frac{f}{sin(\Phi)} & u_0 \\
0 & 0 & 1  
\end{bmatrix}
```

The skew is demonstared by the following graph, where only the phi angle was changed

<img src="./homework submittions/skewGraph.png" width="70%">

</details>

## X] Lecture 3, slide 1 (1 point) (that is duplicate to number 2)


## 4] optional - Lecture 3, slide 4 (1 point), SUBMITTED
[lecture 3 pdf](../pdf/lecture3.pdf)

<details>
  <summary>Click to expand!</summary>

Multiply known matrices K, R and then decompose back; discuss numerical errors

 - K - upper triangular with positive elements
 - R - orthogonal matrix $`(R * R^\top) = I`$


Lets choose

```math
K=
\begin{bmatrix}
1 & 2 & 3 \\
0 & 1 & 2 \\
0 & 0 & 1
\end{bmatrix}
```

```math
a = Pi,
R=
\begin{bmatrix}
1 & 0 & 0 \\
0 & cos(a) & -sin(a) \\
0 & sin(a) & cos(a)
\end{bmatrix}
=
\begin{bmatrix}
1 & 0 & 0 \\
0 & -1 & 0 \\
0 & 0 & -1
\end{bmatrix}

```

```math
K * R
=
\begin{bmatrix}
1 & -2 & -3 \\
0 & -1 & -2 \\
0 & 0 & -1
\end{bmatrix}

```

Then QR decomposition of K * R matrix is according to [online tool](https://www.adrianstoll.com/linear-algebra/qr-decomposition.html)

```math
Q
=
\begin{bmatrix}
1 & 0 & 0 \\
0 & -1 & 0 \\
0 & 0 & -1
\end{bmatrix},
R
=
\begin{bmatrix}
1 & -2 & -3 \\
0 & 1 & 2 \\
0 & 0 & 1
\end{bmatrix}
```


The decomposition can be done using the Gram-Schmidt process (PDF with nice short explanation and example, [here](../pdf/gramSchmidt.pdf).

#### The Gram-Schmidt process
Having a matrix A, where $`a_1`$ up to $`a_n`$ are columns of matrix A.

```math
A
=
\begin{bmatrix}
a_1 \\
a_2 \\
... \\
a_n
\end{bmatrix}
```

Then
```math
u_1 = a_1, e_1 = \frac{u_1}{||u_1||},
```
```math
u_2 = a_2 - (a_2.e_1)*e_1, e_2 = \frac{u_2}{||u_2||}
```

> Please note, that $`a_2.e_1`$ is **dot product**, and * is multiplication of vectors

```math
u_{k+1} = a_{k+1} - (a_{k+1}.e_1)*e_1 - ... - (a_{k+1}.e_k)*e_k, e_{k+1} = \frac{u_{k+1}}{||u_{k+1}||}
```

And

```math
Q
=
\begin{bmatrix}
e_1 & ... & e_n
\end{bmatrix},
R
=
\begin{bmatrix}
a_1.e_1 & a_2.e_1 & a_3.e_1 \\
0 & a_2.e_2 & a_3.e_2 \\
0 & 0 & a_3.e_3
\end{bmatrix}
```

#### The calculation
I will use Gram-Schmidt algorithm.
```math
u_1 = a_1 = (1, 0, 0)^\top
```
```math
e_1 = \frac{u_1}{||u_1||} = \frac{(1, 0, 0)^\top}{1} = (1, 0, 0)^\top
```
```math
u_2 = a_2 - (a_2.e_1)^\top*e_1 = (-2, -1, 0)^\top - ((-2, -1, 0)^\top.(1, 0, 0)^\top) * (1, 0, 0) ^\top
=
(-2, -1, 0)^\top - (-2, 0, 0)^\top
=
(0, -1, 0)^\top
```
```math
e_2 = \frac{u_2}{||u_2||} = \frac{(0, -1, 0)^\top}{1} = (0, -1, 0)^\top
```
```math
u_3 = a_3 - (a_3.e_1)^\top*e_1 - (a_3.e_2)^\top*e_2= (-3, -2, -1)^\top - (-3 * (1, 0, 0)^\top)
-
2 * (0, -1, 0)^\top = (-3, -2, -1)^\top - (-3, 0, 0)^\top - (0, -2, 0)^\top = (0, 0, -1)^\top
```
```math
e_3 = \frac{u_3}{||u_3||} = \frac{(0, 0, -1)^\top}{1} = (0, 0, -1)^\top
```

Therefore
```math
Q
=
\begin{bmatrix}
1 & 0 & 0 \\
0 & -1 & 0 \\
0 & 0 & 1
\end{bmatrix},
R
=
\begin{bmatrix}
1 & -2 & -3 \\
0 & 1 & 2 \\
0 & 0 & 1
\end{bmatrix}
```

As I consulted with mr. Šára, he mentioned, that it would be good to implement this and generate
random matrices and observe the errors.
I did so, and the result can be seen here:
<img src="./homework submittions/grammSchmidtRes.png" width="40%">


>  so I wrote a python application generating random matrices, and computing avg errors
for Gramm schmidt.

So we can see, that this numerically unstable ([wiki](https://en.wikipedia.org/wiki/Gram%E2%80%93Schmidt_process)) algorithm gives us large errors for matrix K,
but small ones for matrix R (the number is * 10 powered to -18)

The source code can be seen here
```python
import numpy as np;
import math;
import random;

def genRMatrix():
    a = math.pi;
    R = [[1, 0, 0],
           [0, math.cos(a), -1 * math.sin(a)],
           [0, math.sin(a), math.cos(a)]];
    return R;

def genKMatrix():
    max_ = 1000;
    # upper triangular with positive elements
    K = [];
    K.append([random.randint(1, max_), random.randint(1, max_), random.randint(1, max_)])
    K.append([0, random.randint(1, max_), random.randint(1, max_)])
    K.append([0,0, random.randint(1, max_)])
    return K;

def getMatrixCol(A, n):
    return np.array(A).T[n];
def norm(vec):
    a = 0;
    for i in range(len(vec)):
        a = a + vec[i] * vec[i];
    return math.sqrt(a);

def calcE(u):
    uNorm = norm(u);
    e = u;
    for i in range(len(e)):
        e[i] = e[i] / uNorm;
    return e

def grammSchmidt(A, DEBUG = False):
    a1 = getMatrixCol(A, 0);
    a2 = getMatrixCol(A, 1);
    a3 = getMatrixCol(A, 2);

    u1 = a1;
    e1 = calcE(u1);
    if(DEBUG):
        print("u1")
        print(u1)
        print("e1")
        print(e1)

    u2 = a2 - np.dot(a2, e1) * e1
    if(DEBUG):
        print("u2")
        print(u2)
    e2 = calcE(u2);
    if(DEBUG):
        print("e2")
        print(e2)

    u3 = a3 - np.dot(a3, e1) * e1 - np.dot(a3, e2) * e2

    if(DEBUG):
        print("u3")
        print(u3)
    e3 = calcE(u3);

    if(DEBUG):
        print("e3")
        print(e3)
    Q = [];
    for i in range(3):
        Q.append([e1[i], e2[i], e3[i]])

    R = [];
    R.append([np.dot(a1, e1), np.dot(a2, e1), np.dot(a3, e1)])
    R.append([0, np.dot(a2, e2), np.dot(a3, e2)])
    R.append([0, 0, np.dot(a3, e3)])

    if(DEBUG):
        print(Q)
        print(R)
    return [Q, R];

K = [[1, 2, 3],
     [0, 1, 2],
     [0, 0, 1]];
R = [[1, 0, 0],
     [0, -1, 0],
     [0, 0, -1]];
A = np.array(K) @ np.array(R);

assert (getMatrixCol(A, 0) == [1, 0, 0]).all();
assert (getMatrixCol(A, 1) == [-2, -1, 0]).all();
assert (getMatrixCol(A, 2) == [-3, -2, -1]).all();

decomposed = grammSchmidt(A);
decR = decomposed[0]
decK = decomposed[1]
assert (np.array(decR) == np.array([[1, 0, 0], [0, -1, 0], [0, 0, -1]])).all();
assert (np.array(decK) == np.array([[1, -2, -3], [0, 1, 2], [0, 0, 1]])).all();


"""
print("decomposed K")
print(decK)
print("decomposed R")
print(decR)

print("error K")
"""
errK = np.average(np.absolute(np.array(decK) - np.array(K)))
#print(errK)
#print("error R")
#print(np.absolute(np.array(decR) - np.array(R)))
errR = np.average(np.absolute(np.array(decR) - np.array(R)))
#print(errR)

errK = 0;
errR = 0;
generateX = 200;
for i in range(generateX):
    R = genRMatrix()
    K = genKMatrix();
    A = np.array(K) @ np.array(R);
    decomposed = grammSchmidt(A);
    decR = decomposed[0]
    decK = decomposed[1]

    errK = np.average(np.absolute(np.array(decK) - np.array(K)))
    errR = np.average(np.absolute(np.array(decR) - np.array(R)))

print("for " + str(generateX) + " random matrices, got errors:")
print("error K")
print(errK)
print("error R")
print(errR)
```

</details>

## 5] optional - Lecture 3, slide 14 (1 point)
Prove (use Cartesian coordinates and L’Hˆopital’s rule)
```math
m_inf = \lim{\lambda \to +\infty}
P *
\begin{bmatrix}
X_0 + \lambda * d \\
1
\end{bmatrix}
=
...
=
Qd
```

## 5] optional - Lecture 3, slide 3 (1 point), SUBMITTED
[lecture 3 pdf](../pdf/lecture3.pdf)

<details>
  <summary>Click to expand!</summary>

<img src="../img/homework3.png" width="40%">


How high is the camera? (tell, whether was the photographer sitting, or standing, etc.)

### Solution
 - the green line is the horizon, as the vanishing points lie on that line
 - therefore, the camera is in the height of the horizon
 - therefore, as can be seen from the people in image, the cameraman is very probably sitting on a chair
 and hold the camera in height of head, or standing, while holding the camera in height of chest
   - because the horizon line is passing where the people have chest


</details>

## 6] Lecture 4, slide 4 (3 point) SUBMITTED, 2.5/3 pts !
[lecture 4 pdf](../pdf/lecture4.pdf)

Feedback
>  Mel byste lepe rozlisovat co je dvojpomer ve 3D (kde nektere delky jsou nekonecne) a co je dvojpomer v obrazku, kde jsou vsechny delky konecne.

<details>
  <summary>Click to expand!</summary>

What is the ratio of the heights of building **A** to building **B**?
Assume you can identify the building bottom and top point.

<img src="../img/homeworkBuildings.png" width="40%">

in image:
`t_a`, `t_b` are tops of building
`u` is horizon

 - What are the interesting properties of line `h` connecting the top $`t_B`$ of
building **B** with the point `m` at which the horizon intersects the line `p`
joining the foots $`f_A`$, $`f_B`$ of both buildings?
 - how do we actuallly get the horizon $`n_{inf}`$
 - give the formula for measuring the length ratio


> example of the final test question, or midterm question


So I used jupyter notebook to calculate the vanishing points

<details>
  <summary>Click to expand!</summary>

```python
# Funkce compute_vanishing_points vykresluje obrázky pomocí knihovny matplotlib,
# která očekává kanály v pořadí RGB a ne BGR jako OpenCV
img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB) ###
vps = compute_vanishing_points(img_rgb)
```
```python
vp1 = [-5.703384625568843, -123.68434833989073, -0.04163054471218133]
```
<img src="../img/homeworkBuildings_1.png" width="10%">

```python
vp2 = [-196.65911940533806, -14.299234707058531, -0.274721127897378]
```
<img src="../img/homeworkBuildings_2.png" width="40%">

```python
vp3 = [43.45824040288957, 46.99825388713831, 0.20823608730874932]
```
<img src="../img/homeworkBuildings_3.png" width="40%">

</details>

##### Solution

<img src="../img/homeworkBuildings_4.png" width="40%">

We assume that the point $`z`$ is at infinity, as $`z_\infty`$.
###### 1)
In the reality, the lines `p` and `h` are parallel. Also, the lines
$`t_B f_B`$ and $`t_A f_A`$ were parallel in the reality.
Therefore, the length of $`t_B f_B`$ and $`u f_A`$ equal.
The point $`m`$ lies on the horizon, and thanks to point $`m`$, we can use cross ratio
on points $`t_A`$ $`u`$ $`f_A`$ $`z`$.

Also, if we might visualize the line $`m f_A`$ to get a new point $`X`$, as here:

<img src="./homework submittions/buildings/building_illustration1.png" width="40%">

Then it would apply, thanks to the cross ratio that
```math
[X t_b f_B z] = [t_A u f_A z]
```
Where the square brackets denote the cross ratio

###### 2) how do we actuallly get the horizon $`n_{inf}`$
We constuct the horizon by drawing lines (vanishing lines), which are parallel in reality (were),
allthough they might no longer be parallel in the image, after the transofrmation happen.

Here is an illustration:

<img src="./homework submittions/buildings/building_illustration2.png" width="40%">

 - blue are the vanishing lines
 - green is the horizon

###### 3)
Using a cross ratio:
```math
[t_A*u*f_A*z_\infty] = \frac{|t_A f_A|}{|u t_A|} * \frac{|z_\infty u|}{|f_A z_\infty|}=\frac{|t_A f_A|}{|u t_A|}
```
This can happen, because we take the limit of  $`\frac{|z_\infty u|}{|f_A z_\infty|}=\frac{\infty}{\infty}`$, which is 1

Let's define $`h_A = |f_A t_A|`$ and $`h_B = |f_B t_B|`$

Knowing that
```math
|u t_A| = |f_A t_A| - |f_B t_B| = h_A - h_b
```
Then
```math
[t_A*u*f_A*z_\infty] = \frac{h_A}{|u t_A|} = \frac{h_A}{h_A - h_B} = \frac{1}{1 - \frac{h_B}{h_A}}
```
In the last step, we divide the fraction by $`H_a`$


```math
[t_A*u*f_A*z_\infty] = \frac{1}{1 - \frac{h_B}{h_A}}
```
Then
```math
\frac{h_B}{h_A} = 1 - \frac{1}{[t_A*u*f_A*z_\infty]} = 1 - \frac{1}{\frac{h_A}{|u t_A|}}
```

</details>

## 7] optional - Lecture 6, slide 1 (1 point)
[lecture 6 pdf](../pdf/lecture6.pdf)

Finish the equation
```math
F = Q_2^{-\top} * Q_1^\top * [e_1]_x = Q_2^{-\top} * Q_1^\top * [-K_1 * R_{21}^\top * t_{21}]_x = ... = K_2^{-\top} * [-t_{21}]_x * R_{21} * K_1^{-\top}
```

<details>
  <summary>Click to expand!</summary>
  
  We know the following:
  - $`K_1`$ is a 3x3 matrix
  - $`R_{21}`$ is a 3x3 matrix
  - $`t_{21}`$ is a 3x1 matrix
  - $`t_{21} = t_2 - R_{21}*t_1`$
  - $`t_{1}`$ is a 3x1 matrix
  - $`t_{2}`$ is a 3x1 matrix
  
TODO

</details>

## 8?] optional - Lecture 6?, slide 5 (1 point)
[lecture 6 pdf](../pdf/lecture6.pdf)

Finish the proof:
```math
A = UB [s]_x * W^\top * V^\top = ... = UB(VW)^\top * [v_3]_x
```

 - you use the definition of $`[X]_x`$ to rewrite it
 - the task is to turn $`[S]_x`$ to V

## 9] optional - Lecture 7, slide 1 (1 point)
[lecture 7 pdf](../pdf/lecture7.pdf)

verify this equation from $`E = UDV^\top`$


<details>
  <summary>Click to expand!</summary>
  
  We know the following:
  - $`(ABC)^\top = C^\top B^\top A^\top`$

```math
E E^\top E = UDV^\top * (UDV^\top)^\top * UDV^\top = UDV^\top * V^\top D^\top U^\top * UDV^\top
```
  
TODO

</details>


## 10] optional - Lecture 7, slide 4 (1 point)
[lecture 7 pdf](../pdf/lecture7.pdf)

Why did we decompose $`D`$ here, and not $`Q = D^\top D`$?

## 11] Lecture 7, slide 12 (2 points), SENT, WAITING FOR VALIDATION
[lecture 7 pdf](../pdf/lecture7.pdf)

Solution can be found [here](./hw_fundamentalVerify.md)

## 12] optional - Lecture 7, slide 15 (1 point)
[lecture 7 pdf](../pdf/lecture7.pdf)

derive $`e_i`$

## 13] optional - Lecture 8, slide ? (1 point)

## 14] optional - Lecture 9, slide 20 (1 point)
