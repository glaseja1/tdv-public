[lecture 7 pdf](./pdf/lecture7.pdf)

```math
P_1 =
\begin{bmatrix}
I & 0 \\
\end{bmatrix}
```

```math
P_2 =
\begin{bmatrix}
[e_2]_xF + e_2e_1^\top & e_2 \\
\end{bmatrix}
```

Given rank-2 matrix F, let e1, e2 be the right and left nullspace basis vectors of F, respectively. Verify that such F is a fundamental matrix of P1, P2 from (17).

Hints:
 -  consider $`xˆi = P_1 * X_i`$ and $`yˆi = P_2 * X_i`$
 -  a is skew symmetric iff $`x^\top * A * x = 0`$ for all vectors $`x`$

> Prove that given matrix F, that what I get are 2 cameras, and their fundamental matrix is the F.

##### solution
We know that for fundamental matrix 3x3 $`F`$, the following must apply:
 - $`F`$ is rank 2 (source: lecture 6, slide 5)
 - $`F`$ is skew symmetric (source: hint)

Lets define some arbitrary matrix $`F`$:
```math
F=\begin{bmatrix}
f_{11} & f_{12} & f_{13} \\
f_{21} & f_{22} & f_{23} \\
f_{31} & f_{32} & f_{33} \\
\end{bmatrix}
```

Now, a matrix $`A`$ is skew symmetrix, iif:
```math
A^{\top} = -A
```
source: general knowledge, or [https://en.wikipedia.org/wiki/Skew-symmetric_matrix](https://en.wikipedia.org/wiki/Skew-symmetric_matrix)

So we narrow our matrix F to
```math
F=\begin{bmatrix}
0 & f_{12} & f_{13} \\
-f_{12} & 0 & f_{23} \\
-f_{13} & -f_{23} & 0 \\
\end{bmatrix}
```
To simpify even further, we can write
```math
F=\begin{bmatrix}
0 & a & b \\
-a & 0 & c \\
-b & -c & 0 \\
\end{bmatrix}
```

Now we also want it to be rank 2, so we can make 2 rown linearly dependent, or just nullyfy one row.
This gives us 3 options:
```math
F_1=\begin{bmatrix}
0 & 0 & f_{13} \\
0 & 0 & 0 \\
-f_{13} & 0 & 0 \\
\end{bmatrix}
```
>  here we set $`f_{12}`$ and $`f_{23}`$ to 0

or set $`f_{13}`$ and $`f_{23}`$ to 0
```math
F_2=\begin{bmatrix}
0 & f_{12} & 0 \\
-f_{12} & 0 & 0 \\
0 & 0 & 0 \\
\end{bmatrix}
```

or set $`f_{12}`$ and $`f_{13}`$ to 0
```math
F_3=\begin{bmatrix}
0 & 0 & 0 \\
0 & 0 & f_{23} \\
0 & -f_{23} & 0 \\
\end{bmatrix}
```

For this purpose, let's work with matrix $`F_2`$ for now.

As e1, e2 is right and left nullspace (told in assigment), it applies:
```math
F*e2 = 0
```
```math
e1 * F = 0
```
Therefore

```math
e_1 =
\begin{bmatrix}
0 \\
0 \\
e_{13}
\end{bmatrix}
```
```math
e_2 =
\begin{bmatrix}
0 \\
0 \\
e_{23}
\end{bmatrix}
```
Where $`e_{x3}`$ is understood as **third** element related to vector $`e_x`$

>  From now on, I will reffer to a fundamental matrix **F**, understanding, that we can then use matrix $`F_1`$,$`F_2`$ or $`F_3`$

Now, if matrix $`F`$ is fundamental, then for all correspondencies, $`x1'`$, $`x2'`$, an epipolar constraint must apply:
```math
x_1'^{\top} . F . x_2' = 0
```

> please note, that the vectors must be understood as rows or columns, so that the multiplication is possible

Now we do not have any correspondecies, but being lucky to be given projection matrices of the camera pair,
lets define some 3D point X:
```math
X = (a, b, c)
```

We know that
```math
P_1 =
\begin{bmatrix}
1 & 0 & 0 & 0\\
0 & 0 & 0 & 0\\
0 & 0 & 0 & 0\\
\end{bmatrix}
```

```math
|e_2|_x=
\begin{bmatrix}
0 & -e_{23} & 0 \\
e_{23} & 0 & 0 \\
0 & 0 & 0
\end{bmatrix}
```
Now, taking matrix $`F_2`$ into consideration, we get
```math
P_2 =
\begin{bmatrix}
e23 f12 & 0 & 0 & 0\\
0 & e23 f12 & 0 & 0\\
0 & 0 & e13 e23 & e23\\
\end{bmatrix}
```

Lets now project our 3D point $`X`$ using the projection matrices. Like that, we get correspondencies:
```math
x_{projected1} = P_1 * X =
\begin{bmatrix}
a \\ b \\ c \\
\end{bmatrix}
```
```math
x_{projected2} = P_2 * X =
\begin{bmatrix}
a*e_{23}*f_{12} \\
b*e_{23}*f_{12} \\
c*e_{13}*f_{23} \\
\end{bmatrix}
```

Now, we have **general** correspondecny pair. So, we verify the stated epipolar constraint:
```math
x_{projected1} * F * x_{projected2} = 0
```
```math
x_{projected2} * F * x_{projected1} = 0
```
And it really results in 0, so it works.

Using Wolfram mathematica, we can see that the epipolar constraint holds (this is just case F_2):

<img src="../img/fundamentalVerify1.png">

I also wrote a program, generating random Fundamental matices, veryfying the calculation explained above.
The functions **setF1**, **setF2**, **setF3** are allowing to test all the cases, and for all, the epipolar constrant holds.

```python
import numpy as np
import random

# the F matrix must be skew symmetric
max_ = 1000;
f11 = 0;
f12 = 0
f13 = 0;
f21 = 0
f22 = 0;
f23 = 0;
f31 = 0;
f32 = 0;
f33 = 0;

e23 = 0
e13 = 0

def setF1():
    f13 = random.randint(1, max_)
    f31 = -f13
def setF2():
    f12 = random.randint(1, max_)
    f21 = -f12;
def setF3():
    f23 = random.randint(1, max_)
    f32 = -f23;

setF2();

a = random.randint(1, max_);
b = random.randint(1, max_);
c = random.randint(1, max_);

# f must be skew symetric
F = np.array([[f11, f12, f13], [f21, f22, f23], [f31, f32, f33]]);
print('F: {}'.format(F))
P1 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]]);
e2x = np.array([[0, -e23, 0], [e23, 0, 0], [0, 0, 0]]);
e2 = np.array([[0], [0], [e23]]);
e1T = [[0, 0, e13]];
X = [[a], [b], [c], [0]];
xprojected1 = P1@X;
P2 = e2x@F + e2@e1T;

print(P2)
# add column
P2 = np.hstack( ( P2, e2 ) )
print(P2)

#P2 = [[-f21, -f22, -f23, Part[e2, 1, 1]], [f11, f12, f13,
#   Part[e2, 2, 1]], [0, 0, e13 e23, Part[e2, 3, 1]]]
xprojected2 = P2@X;

print(xprojected1)
print(xprojected2)
#epipolar constraint must apply
res = xprojected1.reshape(1, 3)@F@xprojected2.reshape(3, 1);
print(res[0][0])
if(res[0][0] == 0):
    print('epipolar constraint ok')
else:
    print('epipolar constraint NOT ok')
```
