# Problem Zoo

Simple intrinsic calibration methods
- Projection Center; Optical Ray, Axis, Plane (s41)
- Vanishing point & Vanishing lines (s43)
- Cross Ratio, 1D projective coordinates (s47)
- **Camera Calibration from Vanishing Points and Lines** (s55)
    - Given 3 vanishing points or vanishing lines
    - Compute `K`
- **Camera Orientation from Two Finite Vanishing Points** (s58)
    - Given `K` and 2 vanishing points with ortogonal known directions `d`
    - Compute `R`
**6-pt alg for camera resection** (s62)
- Given 6x`(X, m)`
- Compute `P`
- Degenerate cases:
    - Points lie on a plane
    - Other configurations unlikely to occur (s65)
P3P: 3-pt alg
- **Exterior orientation** (calibrated resection, P3P) (s66)
    - Given 3x`(X, m)`, `K`
    - Compute `R`, `C` (t)
    - Degenerate cases: `X`s lie on a circle (s68)
- **Relative orientation** (s70)
    - Given 3x`(X, Y)`
    - Compute `R`, `t`
**7-pt alg for fundamental matrix** (s84)
- Given 7x`(m, m')`
- Compute `F`
- Degenerate cases: (s86)
    - Homography related images, camera centers coincide, camera moves but all 3D points lie in a plane
    - Both camera centers and all 3D points lie on a ruled quadric
**5-pt alg for essential matrix** (s88)
- Given `K`, 5x`(m, m')`
- Compute `R`, `t`
**Essential matrix decomposition to rotation and translation** (s82, see `E2Rt(E)`)
**Efficient accurate triangulation** (s89)
- Given `P1`, `P2`, `m1`, `m2`
- Compute `X`
**Robust matching by RANSAC sampling**
- MH Sampler (s119)
- RANSAC (s126)
**Camera system reconstruction** (initial s131, s133)
- Given `E_ij`
- Compute `P_i`, for all `i = 1...k`
- 3 algs to obtain `R_i`, 1 alg to obtain `t_i`
**Bundle adjustment** (s139)
- Given `X_i`, `P_j`, `m_ij` (fixed tentative projections)
- Compute corrected 3D points `X'_i`, corrected cameras `P'_j`
- Minimizes *projection error*: `e_ij(X_i, P_j) = (P_j @ X_i) - m_i`


```python
def E2Rt(E):
    U, D, Vt = np.linalg.svd(E)
    if np.abs(D[0] - D[1]) > 1e-6:
        return []

    Rts = []
    for alpha, beta in [(1, 1), (1, -1), (-1, 1), (-1, -1)]:
        W = np.zeros((3, 3))
        W[0, 1] = alpha
        W[1, 0] = -alpha
        W[2, 2] = 1
        R = U @ W @ Vt
        t = -beta * U[:, 2]
        Rts.append((R, t))

    return Rts
```
