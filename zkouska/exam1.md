Here can be found breakdown of existing exam from previous years.

For questions that have **(OK)** at the end, it means, that an answer was found for it.

<img src="./img/2022.pdf" width="40%">


### 1.
There is a perspective camera mounted on our car, looking out. We have an image from the
camera (below). By analyzing the image of the scene we are able to identify the vanishing line of
the ground plane nh (red horizontal line) and two independent finite vanishing points (intersection
vg of the green line and intersection vb of blue lines). From this we reason that the camera optical
axis is not parallel to the ground plane but it is tilted downwards. Please verify the reasoning. How
would you actually measure the tilt angle α?
Hints:
(1) Assume the tilt angle is the angle between the optical axis and the normal vector of the ground
plane (in case the camera was looking parallel to the ground the tilt angle would be α = π / 2).
(2) We can safely assume the principal point is in the image center x0 (intersection of dashed
yellow lines) and that camera pixels are square. (8 points)


<img src="./img/2022-q1.png" width="40%">

<details>
  <summary>Click to expand!</summary>

</details>

### 2. (OK)
We are given the fundamental matrix F of a camera pair. How would you check that the optical axis
of the two cameras intersect in the 3D space? How would you check they are parallel? How would
you check that they coincide? (4 points + 1 bonus point for quality answer)



<details>
  <summary>Click to expand!</summary>

We have $`(F=(Q_2Q_1^{-1})^{-\top}[e_1]_x = K_2^{-1}[t_21]_xR_{21}K_1^{-1}`$.

if optical axes are palrallel:
 - epipoles e_1 and e_2 will be an infinity since the **baseline is paralel to the image planes**

```math
e_1 = null(F) = (e_11, e_12, 0)
``` 
```math
e_2 = null(F^\top) = (e_21, e_22, 0)
``` 

if optical axes intersect in 3D space:
```math
o = o_2^\top F o_1
``` 
>  epipolar constraint holds

if optical axes coincide:
 - camera centers lie on the same line

```math
e_1 \cong null(F) = o_1
``` 
```math
e_2 \cong null(F) = o_2
``` 

</details>

### 3.
In what condition is M = [b]x a fundamental matrix? In case it can be a fundamental matrix, what
does it tell us about the relative rotation and translation of the cameras? (2 points)


<details>
  <summary>Click to expand!</summary>


</details>

### 4.
We have a camera with a projection matrix P that rotated about its optical center. The camera
projection matrix becomes P’ after the rotation. What is the fundamental matrix of camera pair (P,
P’)? (1 point + 1 bonus point for quality answer)

<details>
  <summary>Click to expand!</summary>

</details>

### 5.
Utility of the 8-point algorithm in RANSAC optimization. Instead of using the 7-point algorithm for
estimating the fundamental matrix, we could use the 8-point algorithm: Eight correspondences give
a data matrix D for the problem Df = 0. We obtain a solution as follows: (1) Let USV
T be the SVD
decomposition of D. (2) We extract the singular vector v0 od D that corresponds to the smallest
singular value σ0
(the singular value is zero when there are no errors in data), hence we set f0 = v0
and we reshape it to a 3x3 matrix F0
. (3) If rank F0 ≤ 1 then there is no unique solution. (4) If rank
F0 = 2 we continue as in the 7-point algorithm. (5) If rank F0 = 3 then we use SVD: Let AB0C
T be
the SVD decomposition of F0
. We construct B from B0 by replacing the smallest singular value in
B0 with zero. Then the result is F = ABC
T
.
Discuss the advantages/disadvantages of the 7-point algorithm over the 8-point algorithm.
Consider also the use of these algorithms in RANSAC. Give more than a single argument. (3
points)

<details>
  <summary>Click to expand!</summary>


</details>

### 6.
Is it possible to replace consensus (the inlier count) with a robust error function in RANSAC? Do
you see any advantages/disadvantages of such a replacement? (2 points)

### 7.
Describe some minimal representation for rotation in 3D space. What is meant by ‘minimal
representation’? Is a rotation matrix such a minimal representation? Justify your answer. (2 points)

### 8. 
A stereoscopic pair is created by translating a camera along its optical axis. Is it possible to rectify
the epipolar geometry of the images by rectification homographies? If so, how would you
implement such rectification? (3 points)

### 9. 
You are designing a stereoscopic perception system with two parallel cameras (the cameras are
mounted with their optical axes parallel to each other and their centers of projection on the line
orthogonal to both optical axes; both cameras have the same focal length and the stereo baseline
is horizontal). The camera CCD chips have the resolution of n = 1000 pixels horizontally and they
have pixels μx = 4.6 μm wide. The focal length of the lenses is 4.6 mm (this corresponds to the field
of view of about 53°). What is the required length of the stereoscopic baseline b in meters so that
an object at the distance z = 100 m has a disparity of at least d = 10 pixels? What distance,
approximately, will then correspond to the disparity of d’ = 11? (2 points)