Here can be found breakdown of existing exam from previous years.

The actual exam can be found in [exam1.md](exam1.md).

For questions that have **(OK)** at the end, it means, that an answer was found for it.

<img src="./img/2015.png" width="40%">


### 1. (OK?)
List the four basic stereoscopic matching methods (constraints), explain their meaning and discuss their advantages and their shortcomings.

<details>
  <summary>Click to expand!</summary>
  This can be found on slide **180**.

  The methods are:
  - uniqueness
  - monotonic
  - coherence
  - piecewise

</details>

### 2. (OK?)
Explain the difference between the algebraic error and the reprojection error for the problem of fundamental matrix estimation. Discuss their relative merits. Is there a case when one error guarantees something about the other?
How is the Sampson error correction related to these two errors (algebraic, reprojection)?


<details>
  <summary>Click to expand!</summary>

  - reprojection error is normalized
  - algebraic error can be easily get from pixels
  - reprojection error is not linear, and is better
  - algebraic error is optimized using triangulation, then I get the model, and that is validated using reprojection error
  - Sampson error actually measures, how much is the epipolar constraint violated

</details>

### 3. (OK)
Describe the following geometric problems by stating what is given, what are
the assumptions and what is the derived result. Do not explain any details of the methods.
(1) camera resection
(2) exterior camera orientation
(3) relative orientation
(4) triangulation

<details>
  <summary>Click to expand!</summary>

  ##### (1) camera resection
  > Source: slide **62**

  - we are given 6 world-image correspondences
  - we calculate projection matrix **P**

  ##### (2) exterior camera orientation
  > Source: slide **69**

  - we are given calibration matrix **K** and 3 world-image correspondences
  - we calculate rotation matrix **R** and translation **t**

  ##### (3) relative orientation
  > Source: slide **69**

  - we are given 3 world-world correspondences
  - we calculate rotation matrix **R** and translation **t**

  ##### (4) triangulation
  > Source: slide **94**

  - we are given projection matrices **P1**, **P2** and 1 image-image correspondence
  - we calculate 3D point **X**

</details>

### 4. (OK)
What is the purpose of the bundle adjustment?

<details>
  <summary>Click to expand!</summary>

  I have key points (used for determining correspondences).
  Based on the 3D model I got so far (can be point cloud) and more cameras I got,
  I remove the noise, by adjusting the key points.

</details>

### 5. (OK)
How do you perform epipolar rectification of a camera pair? What information on the cameras is needed for your rectification algorithm?

<details>
  <summary>Click to expand!</summary>

We need two homographies, Ha and Hb. Such homographies transform the images in such a way that the 
epipolar lines are parallel to the baseline, in order to make the matching algorithmically faster.

We need original matrices of the camera pair. We also do need the fundamental matrix.

</details>

### 6. (OK)
What homographies preserve (do not move) the canonical line at infinity $`l_{inf} = (0, 0, 1)`$?
Derive the result for a general homography.

This is duplicate to the midterm question

### 7. (OK)
What is epipolar rectification good for? Is it unique?

<details>
  <summary>Click to expand!</summary>

Having epipoles, it is used for rectifying (narovnani) the epipoles.
From colored images, I can make a heat map, and using the heat map I
just dump the black points, and will not add them to the point cloud.

<img src="./img/epipolarRectification.png">

</details>

### 8.
What is the relation between disperity, stereoscopic baseline, focal length and the distance of a point
from the cameras in the standard (rectified) stereoscopic pair? What convenient units shall we use
for these variables in order to make the design of a stereopair easy?

### 9. (OK?)
Can one determine the projection matrix of an uncalibrated camera from a
set of three 3D points and their image projection? How?

<details>
  <summary>Click to expand!</summary>

We might be able to use Three-Point Exterior Orientation, if we had K, and K 
can be retrieved if the points given were vanishing points.
However, we do not know that, so we would need some additional information.
 
</details>
 
 
### 10.
Let there be two cameras in a stereopair. Camera 1 can see the vanishing line
of plane p (it sees the *horizon*) so that the epipole $`e_1`$ sits on the line (see figure below).
What can we say about the orientation of the stereoscopic baseline with respect to plane p?
Will the epipole $`e_2`$ in the second image be constrained in any way? Assume
there is no rotation between camera 1 and camera 2. Does the argument change if we do consider rotation?

<details>
  <summary>Click to expand!</summary>

As there is no rotation between the cameras, they are behind each other (or at infinity).
 - centre of the camera 2 projected itself to the infinity
 - baseline is orthogonal to the vanishing line (horizon)
 - baseline connects $`e_1`$ and $`e_2`$

</details>
