﻿# XP33VID - Trojrozměrné počítačové vidění

This is public repository for my friends in TDV course.
Please, keep in mind that it is not guaranteed that this will continue being updated.

 - vypisován každý rok (ZS)

<img src="img/timetable.png"  width="500">

timetable was taken from [here](https://fel.cvut.cz/cz/education/rozvrhy-ng.B201/public/html/predmety/46/84/p4684606.html)

[courses page - TDV](https://cw.fel.cvut.cz/wiki/courses/tdv/start)
[bila kniha](https://fel.cvut.cz/cz/education/bk/predmety/46/84/p4684606)

[MS teams](https://teams.microsoft.com/dl/launcher/launcher.html?url=%2F_%23%2Fl%2Fteam%2F19%3A7690087680764b5d9d35b4bad6633139%40thread.tacv2%2Fconversations%3FgroupId%3Dd3d4842c-6d59-49b6-8115-1400800d5c11%26tenantId%3Df345c406-5268-43b0-b19f-5862fa6833f8&type=team&deeplinkId=f2d055c7-5e1b-407a-971a-bd4a9a694fd2&directDl=true&msLaunch=true&enableMobilePage=true&suppressPrompt=true)

# location
Karlovo namesti v budove E, mistnost KN:E-112 (1. patro, ze schodu doleva, chodbou za roh, pres chodbove dvere, uplne az na konec chodby)

# study materials
Because this course's materials are may be a bit confusing, here is quite epic video, which sums up
the general idea of transformation, and aldo includes some additional information.

[video](https://youtu.be/fVJeJMWZcq8)

# quick help in matrix multiplication
 
<img src="./img/matrixMultiplicHelp.png" width="100%">

# midterm
The next midtrm is at `23.11 - 14:30`