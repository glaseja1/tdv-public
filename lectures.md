# Lecture 1 - introduction
### lidar
  - red means that the point is higher towards the sky
  - point cloud
  - active sensor

passive sensors are cameras

### running around object and taking photos to recreate te 3D model
pipeline:

1. you do not know where the cameras were, so from the images you try to find images,
that are next to each other, ideally.  
2. ?
3.
4. you may want to create mapping, or reconstruct the 3D model, depends on the usage

### Phase1: sparse image matching
 - detect NSIRs (parts of image that match kindoff)

### Phase2: from matches to correspondences
 - in the video, the shadow of the car sometimes gets the blue color. That is an artifact.
 - **SLAM** - Self Localization Mapping

### Phase3: densification of the point cloud
  - in the video, the shadow of the car sometimes gets the blue color. That is an artifact.

calibration of the camera = internal parameters

# Lecture 1
 - vector is a **column** vector by default in matlab

### Homogeneous coordinates
 - marked as underlined letter
 - additional dimension, number 1 is added
```math
\underline{m} = (x, y, 1)
```
> that additional `1` makes the calculations faster and easier

 - lines and points behave a bit in a same way. For example, line can be represented
 by 3 numbers, and so can the point
 - cross product of two points is a line
 - cross product of two lines is the line intersection


### converting homogeneous to cartesian coordinate
```math
\underline{m} = (\underline{x}, \underline{y}, 1),
m = (\frac{\underline{x}}{\underline{z}}, \frac{\underline{y}}{\underline{z}}, 1)
```


if lambda is normalized, and you input point to the line equation with the normalized lambda,
you get a dstance of the point from the line.
So the equaation of the line must be **normalized**, in order to obtain the disance of the point from the line.

if a point $`m=(u,v)`$ is on a line only and only if

```math
au+bv+c=0
```

This can be written as a dot product

```math
(u,v,1)*(a,b,c) = m^T*n=0
```

> here you can see the usage of the *special 3rd* coordinate

### ideal points (points at infinity)
```math
m = (m_1, m_2, m_0)
```

  - all of the points at infinity are sitting on the line of infinity

### intersection of 2 lines
 - can be solved normally, but also can be calculated using cross product.
```math
m = (n \times n)'
```

 - paralell lines intersect at point of infinity

### Labs
 - you are not uploading the result anywhere. You show it to the teacher.

### Summary
Here is my own summary of the lecture.

The idea is, that in the **3D**, having two planes, a dot product can help to calculate the
coeficients for the line, which is the intersection of these planes.

This idea is just taken to the 2D, where as the 3rd coordinate, number **1** is used,
and the 3rd coordinate is then ignored anyway...


# Lecture 2
 - the underscore signifies that there is an additional coordinate in the point

### homography
It is a non singular linear mapping.

#### defining properties
 - collinear points allign to the collinear points
> that means that just a line of points maps to another line of points
 - conurrent lines map to concurrent lines
> that means that lines that intersect map to lines that intersect
 - point line incidence is preserved
> if a point was on line, the homography does not move the points out of the line

Homography is represented by a 3x3 matrix for 2D.
That matrix is **non-singular** (=regular), called **H**.
The H would be 4x4 matrix for 3D.

#### The homography acts differently for lines, and for points

if the homography transforms points to points, we can use it.
If the homography transforms  lines to lines, we must use $`H ^ {-\top}`$

> the homography behaves differently for lines and points, so we shall not
mix it (mapping lines and points together. Just choose one (ideally points))

#### Mapping a finite 2D point
We want to map a point $`m`$ to $`\underline{m}`$ using homography defined by matrix $`H`$.
 1) extends the pixel coordinates to homogenous coordinates, $`\underline{m}=(u, v, 1)`$
 2) map by homography, $`\underline{m}'=H*\underline{m}`$
 3) if $`\underline{m_3}' \neq 0`$, convert the result $`\underline{m'}=(m_1', m_2', m_3')`$ back to Cartesian coordinates (pixels)

```math
u'=\frac{m_1'}{m_3'},
v'=\frac{m_2'}{m_3'}
```

#### why is it used
If I take a photo, which has a perspective, and then I take another
photo, but after rotating the camera, then the two images differ only by some homography

 - the homography matrix can be computed from the camera calibration.

 - it works only for a plane. If the stuff on image is not a plane it becomes distorted.

```math
H  = K*(R-\frac{t*n ^ \top }{d})*K^{-1}
```

K - 3x3 matrix which contains camera properties (focal length, etc.)
n - normal vector of plane
d - distance of the camera from the plane
t - translation it will use

### Euclidean mapping
Allows to translate and rotate at the same time

```math
H =
\begin{bmatrix}
cos \phi & -sin \phi & t_x \\
sin \phi & cos \phi & t_y \\
0 & 0 & 1
\end{bmatrix}
=
\begin{bmatrix}
R & t \\
0^\top & 1
\end{bmatrix}
```
#### This homography preserves
 1) lengths
 Let
 ```math
 \underline{x_i}' = H * \underline{x_i}
 ```
 Let
```math
 (x_i)_3 = 3
```
Then
```math
 ||\underline{x_2}' - \underline{x_1}'|| = ||H * \underline{x_2}' - H*\underline{x_1}'|| = ||H*(\underline{x_2}' - \underline{x_1}')|| =
 ? =
 ||\underline{x_2}' - \underline{x_1}'||
```

 2) angles
 3) areas

> there is a non-mandatory homework on slide


### Affine mapping
```math
H =
\begin{bmatrix}
a_{11} & a_{12} & t_x \\
a_{21} & a_{22} & t_y \\
0 & 0 & 1
\end{bmatrix}
```

 - paralell lines are paralell after the map
 - ratio of areas is preserved
 - ratio of lengths on **paralell lines**
 - linear combination of vectors are preserved
 - convex hull is preserved
   - for some point set, it is the smallest polygon which contains the points (but convex)
 - line at infinity is preserved (not pointwise)

#### does not preserve
 - lengths
 - angles
 - areas
 - circular points

> Eulidean mapping preserves all properties the affine mappings preserve

```mermaid

graph TD;
    1[Eulidean mapping];
    2[Affine mapping];

    1 --> 2;
```

### homography
Here, the things are no longer nice

> Any 3x3 matrix, non-zero is a homography

<img src="./img/lecture2_1.png" width="200">

In the image, point $`[-2, 1]`$ maps to infinity, as in marked in the following image

<img src="./img/lecture2_2.png" width="200">

> you can see, that the convex hull is not preserved (the rectangle is mapped
to some infinite...something...)

#### the map preserves
 - incidence and concruency
 - collinearity
 - cross ratio (ratio of ratio)

#### the map does not preserve
 - lengths
 - areas
 - pallalelism
 - ratio of areas
 - ratio of lengths
 - linear combinations of vectors
 - convex hull

line `(1, 0, 1)` would be drawn at `-1` and is vertical
> The first 2 coordinates `(1, 2)` are the normal vector

### U-V coordinates
The coordinates are not `x-y`, but `u-v`. **U** is to the **right**, **V** is pointing **down**.
So 1, 1 is the upper-left corner.
 - X goes to the right
 - Y does down
 - Z goes to the front

> Traditionally you have camera and in front of it you make a hole, and view.
The image is, however, upside down.
Ss it is better to put the hole behind the plane, and it will not be then
upside down.

### Pinhole camera in homogeneous form
This matrix allows projection from 3D to 2D

```math
\begin{bmatrix}
\underline{x} \\
\underline{y} \\
\underline{z}
\end{bmatrix}
=
\begin{bmatrix}
f & 0 & 0 & 0 \\
0 & f & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}
=
\begin{bmatrix}
X \\
Y \\
Z \\
1
\end{bmatrix}
```
However, the matrix can be dissasembled to 2 matrices
```math
\begin{bmatrix}
f & 0 & 0 & 0 \\
0 & f & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}
=
\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}
*
\begin{bmatrix}
f & 0 & 0 & 0 \\
0 & f & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
```
The matrix on the **left** is a matrix which converts 3D to 2D, and the matrix on the **right** allows scalling or zooming, using focal length **f**.

### Changing the coordinates to be on a grid
The idea is, that normally, the $`(0, 0)`$ point is in the upper left corner.
We, however, want to shift it to the centre of the camera.

```math
\begin{bmatrix}
\underline{u} \\
\underline{v} \\
\underline{w}
\end{bmatrix}
=
\begin{bmatrix}
\frac{1}{p_u} & 0 & u_0 \\
0 & \frac{1}{p_v} & v_0 \\
0 & 0 & 1
\end{bmatrix}
=
\begin{bmatrix}
\underline{x} \\
\underline{y} \\
\underline{z}
\end{bmatrix}
```

 - $`u_0, v_0`$ are coordinates of the centre of the image
 - $`p_u, p_v`$ scales from meters to pixels

### Complete transformation overview
Here is a complete overview, on how the 3D point is converted to 2D.

<img src="./img/completeCamModel.png" width="100%">

 - **intrinsic parameters** - they depend only on the camera
 - **extrinsic parameters** - they depend on where the camera is, not on the camera parameters
 - `t` - where is the camera in the world

The *extrinsic parameter* matrix can be seen in the following image

<img src="./img/transformMatrix.png" width="100%">

### The rotation matrices
As can be seen in the image, there is a rotation matrix **R**.
>  I do not know what the R matrix contains, as it is not in the slides....

Generally, there are 3 rotation matrices

```math
R_x(\theta) =
\begin{bmatrix}
1 & 0 & 0 \\
0 & cos \theta & -sin \theta \\
0 & sin \theta & cos \theta
\end{bmatrix}
```
```math
R_y(\theta) =
\begin{bmatrix}
cos \theta & 0 & sin \theta \\
0 & 1 & 0 \\
-sin \theta & 0 & cos \theta
\end{bmatrix}
```
```math
R_z(\theta) =
\begin{bmatrix}
cos \theta & -sin \theta & 0 \\
sin \theta & cos \theta & 0 \\
0 & 0 & 1
\end{bmatrix}
```

A general rotation matrix can be created by multiplying
```math
R = R_z(\alpha)*R_y(\beta)*R_x(\phi)
```

The information was taken from [wiki](https://en.wikipedia.org/wiki/Rotation_matrix)

### Natural and canonical image coordinate system
```math
(x', y', 1) = (\frac{x}{z}, \frac{y}{z}, 1) = \frac{1}{z} * (x, y, z) = (x, y, z) =
\begin{bmatrix}
I & 0
\end{bmatrix} *
\begin{bmatrix}
x \\
y \\
z \\
1
\end{bmatrix}
```

> The map (matrix) represents the camera. Maps 3D to 2D

 - $`(x', y', 1)`$ is the position in homogeneous representation
 - $`x, y`$ - the original position of the point
 - $`(\frac{x}{z}, \frac{y}{z}, 1)`$ - how we convert original position to the homogeneous coordinates

the $`\frac{1}{z}`$ can be ignored, because it is the lambda

 - x' and y' are the ratios of the coordinates

3D point would then be [x, y, z, 1]

We then use a matrix to drop the last 1 (that is why the last column is zero)

$`u0, v0`$ is a shift

$`f`$ is the rescalling constant. In fact, it is a focal length (for us, in pixels)
so,
```math
u = f \frac{x}{z} + u_0,
v = f \frac{z}{z} + v_0
```
is a digitization of the image
(we just want to create pixelated image from a world)

someone gives me matrix P, I multiply it with my point to digitize the point
$`K`$  is the calibration matrix

### transforming a point from the world to camera coordinate system

 - we take a point in the camera coordinate system, we rotate it, translate it,
map by K matrix to digitize it, and drop the 3rd coordinate

To transform a point from **world** to **camera coordinates**
```math
X_c = R*X_w+t
```

`R` - 3x1 matrix representing rotation (camera rotation matrix)
  - it is the world orientation of the camera frame
`t` - camera translation vector
    - world coordinate in the camera frame

`C` - camera position in the world reference frame
`P` - projects 3D point to image plane
> the inverse of that would give you a line, on which the point can be

### changing the inner reference frame
### The general form of calibration matrix K
The calibration matrix K is also called intrinsic matrix K on [wikipedia](https://en.wikipedia.org/wiki/Camera_resectioning).

> the idea is that we can take image of a floor, and the take image of that image.
Well, we can parametrize this.

Here is the form of matrix K from the course:

```math
K = \begin{bmatrix}
af & -a*f*cot \phi & u_0 \\
0 & \frac{f}{sin \phi} & v_0 \\
0 & 0 & 1
\end{bmatrix}
```

 - $`\phi`$ - skew angle (skewing) between `x` and `y` axis
 - $`a`$ - pixel aspect ratio
 - $`u_0`$,$`v_0`$ shift for the centre
 - $`f`$ is the focal length (scalling)

so we have 5 parameters, but 11 degrees of freedom

e prime u 1, 0
e prime v 0, 1

Here is the form of matrix K from the wikipedia:

```math
K = \begin{bmatrix}
f * m_x & \phi & u_0 \\
0 & f * m_y & v_0 \\
0 & 0 & 1
\end{bmatrix}
```
 - $`m_x`$, $`m_y`$ are the inverses of the width and height of a pixelon the projection plane
 - $`f`$ is the focal length
 - $`\phi`$ is the skew coefficient between `x` and `y` axis
 - $`u_0`$,$`v_0`$ represent the principal point, which would be ideally in the center of the image


#### homework
What do `f`,`a`,`theta`,`u0`, `v0` mean?
 - `a` is some scalling somewhere. **Where**? Homework
 - $`\phi`$ is skew angle is skew angle, but **where**? Homework

Decompose this large mapping to smaller mappings, such as only translation mapping, or rotation mapping.
Identify what `a` and `theta` actually means

> From a video, I found, that phi is rotation around X axis, theta is rotation around y axis


### decomposition of matrix K
```math
A
=
\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}
```
```math
B=
\begin{bmatrix}
f & 0 & 0 & 0 \\
0 & f & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
```
```math
C=
\begin{bmatrix}
0 & 0 & u_0 & 0 \\
0 & 0 & v_0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0
\end{bmatrix}=?
```
 - matrix `A` converts 3D to 2D
 - matrix `B` allows scalling or zooming, using focal length **f**
 - matrix `C` shifts towards the centre of the image (u0, v0)

```math
\begin{bmatrix}
f & 0 & 0 & 0 \\
0 & f & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}
= A * B =
\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}
*
\begin{bmatrix}
f & 0 & 0 & 0 \\
0 & f & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
```

# Lecture 3
Rotation matrix is 3x3 orthogonal
Inverse of R is transposition.

C is the position of the camera in the world.

> We would like to get K, R from P, if P is given.

we want to go from P=[Q, q] to K[R t]

we are decomposing matrix K, without given Q and R
$`R_31`$ is around one axis
$`R_21`$ is around another axis
$`R_32`$ is around another axis

R32 .. the red s means that it is R32 element

we get system of 2 equations

> This decomposition is however not unique

if we get projection matrix, we can now decompose it to the KR.

### Center of projection
Lets say we have P, and we would like to know where is the position of the camera.


B is a unit vector, but we do not know where it is.
Proof
we choose a point, and connect a line two points A and B
every point on line projects to a same point, PA
so B is on the line, because that is how the projection works

if we choose another point A', then the B is on the line too, therefore, B==C

### Slide: Optical Ray
line is given by point and vector

```math
X(u) = C + miii * Q^-1 * m
```

Q inverse is from the camera

### Slide: Optical Axis
It is optical ray, which is perpendicular to the image plane pi
optical axis is ortogonal to the image plane

Point X is projected at infinity
X is from R3
```math
q3t * X + q34 = 0
```
is a plane

q3 is last row of the Q matrix

### Slide: Principal point
principal point m0 = Q * q3 (last row of the maptrix)

### Slide: Optical plane
p is the normal vector

```math
(Q ..) \times (Q...) = ...
```
the cross product is just prescription, you do not need to understand it.

So to get the plane:
 - we know that p is orthogonal to line defined by X and C.


# train slide
We want to know how far is the train.
We have image of perspective.

*the vanishing point* = point where the tracks intersect
That is a project of a point at infinity (tracks project at infinity, but we see it at image)

## slide vanishing point
## slide vanishing point applications
Theoretically, if we would have a lot of line pairs, we could obtain a line
which contains all the points of infinity (=horizon)

> all paralell planes, when projected, intersect in the same *horizon* line

```math
p = Q^\top * n
```

and if I have several `p` and `n`, I might get the `Q` (=calibration)

## slide cross ratio
 - Lets say we have line `N`.
 - We choose points on line, lets say `R`, `S`, `T`, `U`
 - having camera, I project the points to `r`, `s`, `t`, `u`
 - I can take the distances between the points

I create ratio of ratios (=cross ratio)
```math
[RSTU] = \frac{RT}{SR} * \frac{US}{TU}
```
> We can create 3 more different cross ratios, this is just one of them

I get the same number when I measure it on the image, vs in reality (it preserves ratios)

> the n line is a homography of the N line, but we do not know which homography, there are many

It applies
```math
[RSTU] = [rstu]
```

> This applies ONLY to the perspective, pin hole camera models

we define here that inf / inf = 1

### 1D projective coordinates
I can get the distance in tiles from the image (even if there would be something obscuring the floor.

Projective coordinate in 3D (P) is the same as the projective coordinate in image (p)

##
notes:
v0 v0' between that you get theta...

# Lecture 4?
### slide: application: finding the horizon from repetitions

we want to get distance between P_0 to P_inf in the image

> yellow line seems to be chosen randomly

### slide: Module III - computing with a single camera

> [2] Random Sample Consensus - paper, containing the algorithms discussed

### calibration of the camera
 - photograph an environment
 - identify the lines
 - find vanishing points
 - get the calibration matrix

### slide: ?
 - identify the points
 - use formulas
 - get omega

### slide: contd's

 - 6, 7, 8 are assumptions

You choose from the above conditions, according to your problem,
and get linear equations.

Solving $`D*w = 0`$ is easy, just find the null space.

You get $`K`$ from $`omega^{-1} = K * K^\top`$

For Choleski decomposition, in matlab you use $`K = chol(omega)`$

### slide: Examples

If we know where the principled point is, then we just calculate f.

In case, where one or both points is in the infinity, the equation can
not work, as you get $`f^2 = f^2`$

### slide: camera orientation from two finite vanishing point

$`w_1`$ is the first column of the rotation matrix

> So, for determining the $`Q`$ matrix, we only need 2 vanishing points

### slide: Application: Planar rectification
if we want to go from $`m`$ to $`m'`$,
then the homography is $`H = K * R^\top * K^-1`$

### slide: Camera resection
It is camera **calibration** and **orientation** from a known set $`k \geq 6`$
reference points and their images

### slide: The minimal problem for camera resection
just formulas

the A matrix has 12 rows. That is unsolvable, I need 11 rows.
However, some rows will be equal, and I get 11rows.

The problem can be, that there can be errors, and then the matrix rank is 12.
In that case, i just drop the last line :D.

I can drop all 12 lines one by one, get 12 results, and then average that.
This allows to use **all** the data. (Jack-knife estimation)

> Another way is to calculate P_i and decompose to $`K_i`$, $`R_i`$, $`t_i`$ and then
average the $`K_i`$, $`R_i`$, $`t_i`$.

### slide: Degenerate configurations for camera resection

There are cases
case 1: all 3D points are in one plane. In that case, there is no solution
case 2: all 3D points are in one plane. And there is also one point outside of the plane.
In that case, there is no solution

# Lecture 5
### slide: F3P
 - `m` is the location at image plane of point `x`

is there a way to get z_1 or z_2?

We know the angle, and v1, v2.
 - we get rid of the rotation

### slide: degeneration
the three points are always on a plane

There is always a circle, on which the points lie.
 - if the camera lies on the circle, there is no solution
 - if the camera is raised, above the circle, the solution is unstable

> basically you have no problem with points on a plane, but there are these rare cases.
It is good to just know.


### slide: relative orientation problem
 - we have points (x1, x2, x3), (y1, y2, y3) in general position
 - known correspondence between x and y points

### slide: an optimal algorithm for relative orientation
in the second line, we do not care about how large the set it, so we remove the
3 from the sum

`arg min` is argument that minimizes the cost function (`R*`)

> we have tranformed the problem into optimization

we will try to keep there the R matrix, and we will just try to rearrange it.

 - `Z` and `W` are known vectors, calculated from the different slide

`:` operator is a dot product between matrices
so it is like `vectorized A` dot product `vectorized B`

`tr` - trace of a matrix [wiki](https://en.wikipedia.org/wiki/Trace_(linear_algebra))

obs 3:
trace of a single number is the number


### slide: the algorithm
the solution is

```python
R = USV^\top
```

we are just trying different S matrices
S must be orthogonal, diagonal, and positive definite

### slide: Computing with a camera pair

### slide: geometric model of a camera stereo pair
 - we have 2 cameras, C1, C2.
 - the cameras share some field of view

epipolar plane - plane on which is the point and the 2 cameras
epipole - respective images of the cameras
epipolar line - line defined by `m1` `e1` (in the slide illustration)

### slide: forward motion
Camera is in the height of the handle, because the epipolars intersect
at the handle (there is a vanishing point)

### slide: cross products and maps

corresponding d2 b d1 are coplanar, iif
(d2 x b ) . d = 0

|b|x is a notation
```math
b x m = |b|_x
```
> takes a vector, and constructs a 3x3 matrix
Definition
```math
|b|_x =
\begin{bmatrix}
0 & -b_3 & b_2 \\
b_3 & 0 & -b_1 \\
-b_2 & b_1 & 0
\end{bmatrix}
```


However, we can write it using matrices

skew symmetry:
```math
|b|_x^\top = -|b|_x
```

```math
|b|_x^3 = -||b||^2.|b|_x
```

5 There must be a null space, so the rank is not full, it is 2

9 if we have rotation of the vector and all that cross, we can take the
rotation off


# Lecture 6


### slide: Expressing epipolar constraint algebraically

> there might be a optional homework....check it out...?

```math
0 = d_2^\top * P_e
```

> this is just saying that d_2 is orthogonal to the plane

```math
\underline{m_2}^\top * (Q_2 Q_1 [e1]_x) \underline{m_1}
```
> `m1`, `m2` are image points in the homogeneous cordination

We now have $`\underline{m_1}`$, $`\underline{m_2}`$ in relationship
```math
e_1 = Q_1 C_2 + q_1 = Q_1 C_2 - Q_1 C_1 ...
```
> this is possible, because `q_1` is computable from `Q1*C1`

Essential matrix:
```math
E = [-t_{21}]_x * R_{21}
```

Fundamental matrix:
```math
F = K_2^-\top * [-t_{21}]_x * R_{21} * K_1^-1
```
Fundamental matrix F must be singular

### slide: The structure and the key properties of the fundamental matrix
$`H_e`$ maps epipolar lines from one image to epipolar lines in the other image

Theoretically, decomposing $`F`$ to get $`H_e`$ might be possible, but it
is very difficult.

Instead, we can get matrix for such a map using equation:
```math
l_2 = F[e1]_x l_1
```
 - $`F[e1]_x`$ is, however, not a homography (because it is singular matrix)
 - $`F[e1]_x`$ is, maps epipolars to epipolars

> This bypasses the necessity to decompose $`F`$

### slide: Representation theorem for fundamental matrices
Definition:
$`F`$ is fundamental when $`F=H^-\top[e_1]_x`$ where $`H`$ is regular and $`e_1 = null`$ and $`F != 0`$

Theorem:
A 3x3 matrix $`A`$ is fundamental iff it is of rank 2


Proof:
> we are proving that fundamental matrix ...?
1] let $`A = UDV^T`$ be `SVD` of $`A`$ of rank 2
Then `D` must look like this:
```math
D = diag(, , 0)
```
> we use `SVD` decomposition
here is a meaning of the `diag(a, b, c)`:
```math
diag(a, b, c) =
\begin{bmatrix}
a & 0 & 0 \\
0 & b & 0 \\
0 & 0 & c
\end{bmatrix}
```

2] we write `$D = BC`$

3]
4]
> is there a matrix W so that it maps C to skew symetric matrix?
5]
6]
We can write A also like this
```math
A = UB [s]_x * W^\top * V^\top = ... = UB(VW)^\top * [v_3]_x
```
 - $`V`$ is orthogonal
 - $`W`$ is rotation, so also orthogonal
 - $`[v_3]_x`$ is matrix composed of $`v_3`$ third column of the V matrix

*we can absorb the W into V*

Theorem:
Let `E` be a $`3x3`$ matrix with `SVD` decomposition $`W=UDV^\top`$. Then E is essential iff D = diag(1, 1, 0).

> so if we want to generate a random essential matrix, we know what to do

### slide: Essential matrix decomposition

What does the alpha and beta mean?
 - change of sign in alpha rotates the solution by 180deg about $`t_21`$

### slide: 4 solutions to the essential matrix decomposition
 - if I change the sign of $`\alpha`$, it rotates the entire setup
 - if I change the sign of $`\beta`$, it is like relocating the cameras (rotating the baseline)

> So, if I have something that tells me, where is **behind** the camera,
I can choose which of the 4 cases happened


### slide: A note of oriented epipolar constraint
We know already
```math
\underline{m_2}^\top * F * \underline{m_1} = 0
```

We have epipolar constraint
```math
(\underline{e_2} \times \underline{m_2}) + F * \underline{m_1}
```

> this can help to reduce to less than the 4 cases in the previous slide

### slide: 7-point algorithm for Estimating fundamental matrix
```math
\underline{y_i}^\top * F * \underline{x_i} = 0, i = 1 ... k
```
We know
```math
\underline{y_i}= ()
```

I would like to see E instead of F there, but the idea is the same...

##### Solution
...

```math
D*vec(F) = 0
```

 - `vec()` takes columns, and puts them on the top of each other (9x9) matrix
 - `D` is the data (7x9) matrix

> but F is fundamental matrix, the math written above is without considering that F is fundamental.
So, F is rank of 2.

> The trick is, that we know that F is somewhere in the 2D nullspace


> Inverse of vectorization is matricization
```math
a = vec(A), A = vec^-1(a) = mat(a)
```


### slide: 7-point algorithm for Estimating fundamental matrix (continuation)
3] The determinant will give me a 3rd degree polynomial in alpha

> So I have 3 solutions from 7 point corespondencies

### slide: XXXX



# Lecture 7
### slide: 5-Point Algorithm for Relative Camera Orientation
> We have less degrees of freedom, so we may use less points (5)

### slide: the triangulation problem
### slide: The Least-Squares Triangulation by SVD
 - compute Q
 - calculate SDV decomposition
 - solution is the last column of **U**

### slide: Numerical Conditioning
In the equations, there is a mistake. There shall be $`\underline{\hat{x}}`$ instead of $`\underline{mX}`$

### slide: Algebraic Error vs Reprojection Error
This is an example to illustrate the solution
 - blue dot is correspondence in one image
 - red dot is correspondence in other image

> it is not good, if we have milion of data, and we want to do it in a reasonable time


### slide: Optimization for 3D Vision
### slide: The Concept of Error for Epipolar Geometry

### slide: Fitting A Circle To Scattered Points
 - my measurements are the points
 - my error function is the distance of point (the epsylon(X))

> The derivative over x is Jacobian



# Lecture 8
### slide: Fitting a circle to scattered points
### slide: Circle fitting: some results
in the image (big radial noise):
 - points are chosen randomly on the circle
 - every point could have been moved from the circle away in direction of a normal
 - blue is the minimizer of the error
 - if we correct the blue error using sampson approximation, it is the red one

in the image (the fourth):
 - the points can be kicked in any direction

So, what would be the best estimator for the isotropic noise?
It is not easy, it is huge insane formula, but is displayed on the slide

> The idea is, that converting some error function to another error function is
not going to work. We shall use the knowledge of used noise for our solution, but
this is impossible in a real world, as the processes are not known and can not be
described by the probability distributions


### slide: Sampson error for Fundamental matrix manifold

### slide: Fitting a circle to scattered X


### slide: Back to triangulation the golden standard method
 > check with video, a student asked ,,how to iterate" and teacher explained more


the red dot makes a better result from the sampson corrected error minimizer

 > however, researchers say: just run it once, do not iterate, it is good,
we have better methods (seen later)

Suppose that we have pair of images. We have point correspondences.
we can estimate Fundamental matrix from correspondences.
We get distorted result, as we do not have calibration.

### slide: Back to fundamental matrix estimation

 - calibrate camera
 - run it with essential matrix
 - correct the result afterwards


### slide: Levenberg-Marquardt
the partial derivative is with respect to the variable, not to the data

we add a matrix, diagonal one, multiplied by some lambda.
The problem is, how do we choose the lambda constant?

 - we guess lambda
 - we look, if it decreased the error
 - if it did not decrease the error, we try to scale down the lambda
 - if it worked, we can make larger step, so we increase the lambda

> in reality, sophisticated methods, such as region methods, are not faster
really

### slide: LM with Sampson error for fundamental matrix estimation
a problem can be that one measurement can be so wrong, that it skews the
good solution to the bad one.

### slide: Local optimalization for fundamental matrix estimation

### slide: The full problem of matching and fundamental matrix estimation
 - we have set of images.
 - we can find some parts, that we can use as a correspondency point.
= inlier keypoint
 - we get key points in one image, and in other image
 - now, we will try to match the key points to get some correspondencies
   - the correspondencies must be one to one mapping
 - if they are correspondecies, there must be a fundamental matrix
So I can calculate matrix E and F and calculate the error,
to see how well i guessed the correspondencies

```math
(M*, F*) = ...
```

The formula:
 - M* is matching
 - F* is fundamental matrix
 - i will be maximizing the probability density
 - E is epipolar
 - D are descriptors
 - F is fundamental matrix
 - M is the matching

M is a map from left to right, can be represented by a matrix
each row represents one key point, each row is

if row 5 col 6 is 1, then:
keypoint 5 matches keypoibnt 6

inlier keypoint can be a centre of some interest area,
but here we are working with a point


### slide: ?

```math
F* = arg max p(E, D, F)
```

instead of trying all different Ms, I sum a function for all possible Ms,
so I get rid of the M. (=marginalization)

# Lecture 9
### slide: Deriving A Robust Matching Model by Approximate Marginalization

### slide: Robust matching model (cont'd)
the gaussian p_1(e_ij | F) = the eexponent (gaussian) there is the distribution of the epipolar error

p_1 is the name of the distribution for the sake of the presentation

### Simplified Robust Energy (Error) Function


### The Action of the Robust Matching Model on Data
if we add t to the red function, it turns into the green function

the error is robust: it is tolerant to outliers

We just need to choose **t** for the formula.
I express **t** by finding where the 2 distributions overlap

So, I have fundamental matrix F.
I also have 2 sets of points, n and m, which I select,
I do not have point matches, but I can calculate them using this? IDK...

### Metropolis-Hastings (MH) Sampling
 - C is the proposal distribution
 - from se, we sample

we generate random x, evaluate the function, and accept it or reject it

### The Nine Elements of a Data-Driven MH Sampler
 - we have a plane, in which is set of points
 - we consider all possible pairs of the points, and from them, we choose

### cont’d

### Data-Driven Sampler Stopping
 - take the data, and wait, until both points are inliers
 - eps is the proportion of the inliers in the data
 - N is the number of times it failed to choose both inliers
 - reliability: X times in R runs, I want to fail
   - because you never know whether solution is really correct...

# Lecture 10
### slide: RANSAC with Local Optimization and Early Stopping
 - wrong correspondencies could be shown as outliners
 - primitives are the points
 - we randomly select subset (smallest possible) from the primitives
 - estimate parameters (for example equation of a line)
    - theta is the essential matrix or fundamental (depends what algorithm we have, and what we are looking for)
 - I can compute PI(s) which is a function telling me a quality of the solution
 - epsylon is proportion of inliers against the number of primitives


PI(s) should be robust

 > just RANSAC algorithm, but he just explains it in so fucking shitty confusing way  

we want to minimize the error, so we use robust error

### slide: Stripping MH Down To Get RANSAC
 - = metropolis Hastings RANSAC

 > exam has a test, then oral part
17.1 TDV exam

### slide: Reconstructing Camera System by Gluing Camera Triples
> this is the stuff in labs

# Lecture 11
### The Projective Reconstruction Theorem
if we want to reconstruct the system without correspondencies, just from essential matrices,
we run into problems

### Reconstructing Camera System from Pairs (Correspondence-Free)
assume that wehave X cameras, and we have pairwise essential matrices,
decomposed rotation and translations

 - we take P1 an set it to [I 0]
 - next camera has position of [R t]
 - Pij camera has position of [Rij tij]

each camera has local location translation

we want to send them to some global coordinates, for that
we use H (homography) map, but we do not know scale s

### cont’d
we set global coordinates, so for example we fix first camera
R1 = I (from P = [I O])

> unless we have some information about scale, we can not get it

##### poor man's algorithm
if r1 = I
Rij * Ri = Rj
so if Ri = I
we get rotation of the next camera

##### rich man's algorithm

##### SVD lover's algorithm

##### Bundle Adjustment

# Lecture 12
### Stereovision
 - used for densifying the point cloud

> TODO: write down the optional homeworks from this lecture

# Lecture 13
we can work with disparity, as opposed to the epipolar geometry
 - we rectify the image using homography

difficulties:
 - we want to find correspondecies

### binocular disparity in ...
He wants us to understand this one, as it is core mechanic of stereo vision
