Here are some notes and definitions, that might be usefull for the exam

### 1) Inverse of vectorization is matricization
```math
a = vec(A), A = vec^{-1}(a) = mat(a)
```

### 2) $`[]_x`$ operator
from slide **Cross Products and Maps by Skew-Symmetric 3 × 3 Matrices**
```math 
|b|_x=
\begin{bmatrix}
0 & -b_{3} & b_{2} \\
b_{3} & 0 & -b_{1} \\
-b_{2} & b_{1} & 0
\end{bmatrix}
```
where 
```math 
b = (b_1, b_2, b_3)
```
The eigenvalues of $`[b]_x`$ are $`(0, \lambda, -\lambda)`$ (**that is from slide**)

### 3) eigenvalue calculation 
```math 
det(A - \lambda I) = 0
```

### 4) checkerboard rule determinant calculation 
```math
det(
\begin{bmatrix}
a & b & c \\
d & e & f \\
g & h & i
\end{bmatrix}
)
=
a * det(
\begin{bmatrix}
e & f \\
h & i
\end{bmatrix}
)
-
b * det(\begin{bmatrix}
d & f \\
g & i
\end{bmatrix}
)
+
c * det(\begin{bmatrix}
d & e \\
g & h
\end{bmatrix}
)
```

### line n - point x incidency
For incidency must apply
```math 
\underline{x} . \underline{n} = \underline{x}^\top . \underline{n} = 0
```
Where $`.`$ is the dot product
  
# preparation on midterm
This section contains my preparation on the midterm test

### dot product
Given the two vectors $`a=(a_1, a_2)`$ and $`b=(b_1, b_2)`$ the dot product is:
```math
a . b = a_1 * b_1 + a_2 * b_2
```

### cross product
Given the two vectors $`a=(a_1, a_2)`$ and $`b=(b_1, b_2)`$ the cross product is:
```math
a \times b = (a_1 * b_1, a_2 * b_2)
```

### properties of homographic calculations
 - cross product of two homographic lines is their intersection point
 - průsečík dvou rovnoběžných přímek je úběžník
 - přímka vedoucí skrz dva úběžníky je úběžnice
 

|  |  |
| ------ | ------ |
| point | $`p=(x, y, z)`$ |
| incidence | $`p^\top u = 0`$ | 
| collinearity | $`| p_1 p_2 p_3 | = 0`$ | 
| line from points | $` u = p_1 \times p_2`$ | 
| ideal point | $`(x, y, 0)`$ | 

|  |  |
| ------ | ------ |
| line | $`u=(a, b, c)`$ |
| incidence | $`p^\top u = 0`$ | 
| concurrence | $`| u_1 u_2 u_3 | = 0`$ | 
| intersection of lines | $` p = u_1 \times u_2`$ | 
| ideal line | $`(0, 0, c)`$ |

### cross ratio

formula
```math
RTSU = \frac{RT}{SR} * \frac{US}{TU}
```

As long as the 2 lines are paralell, the following ratio also applies:

<img src="./img/examPrepare-1.png" width="40%">

> This information is taken from a [YT video](https://www.youtube.com/watch?v=ffvojZONF_A)

#### example

Suppose the following situation:

<img src="./img/examPrepare-2.png" width="40%">

Then, the following applies:

```math
BCDE = \frac{BD}{CB} * \frac{EC}{DE} = \frac{3}{2} * \frac{4}{3} = \frac{12}{6} = 2
```
```math
FGHI = \frac{FH}{GF} * \frac{IG}{HI} = \frac{5}{3} * \frac{2+x}{x}
```
```math
BCDE = FGHI
```
```math
2 = \frac{10+5x}{3x}
```
```math
6x = 10+5x
```
```math
x = 10
```
So the distance **HI** is 10 units.

### Epipolar geometry 

[Youtube video](https://www.youtube.com/watch?v=cLeF-KNHgwU)

Epipolar geometry describes the relative positioning of two cameras in a stereo system

The idea is, that I see a projection `X_1` of 3D point `X` in camera 1.
But I want to know, where is the point X projected for camera 2.

#### epipole 
Epipole is a projection of the other camera's projection center into the image 

#### epipolar plane
Is defined by the point X and the `?`

#### epipolar line
If I intersect an epipolar plane with an image plane, I get the epipolar line 

#### epipolar contraint
 - lets have 3D point $`X=(x, y, z)`$
 - lets have 2 cameras $`C_1, C_2`$, seeing the point $`X`$
 - $`x_1'`$ is the projection of $`X`$ using camera $`C_1`$

```math
x_1' = P_1 * X
```

 - $`x_2'`$ is the projection of $`X`$ using camera $`C_2`$

```math
x_2' = P_2 * X
```

> $`P_1`$ and $`P_2`$ are projection matrices for the cameras

 - so, $`x_1'`$, $`x_2'`$ are correspondencies
 - epipolar constraint says that the following must apply:
 
```math
x_1' * F * x_2' = 0
```

> where $`F`$ is fundamental matrix

> so it also says that for all correspondencies, the epipolar constraint must hold...


### Affine mapping
```math
H = 
\begin{bmatrix}
a_{11} & a_{12} & t_x \\
a_{21} & a_{22} & t_y \\
0 & 0 & 1
\end{bmatrix}
```
Online, there is also the following version (more general?):
```math
H = 
\begin{bmatrix}
a_{11} & a_{12} & a_{13} \\
a_{21} & a_{22} & a_{23} \\
0 & 0 & a_{33}
\end{bmatrix}
```
 - there are 6 degrees of freedom
 - for calibration, we need minimum of 3 points

### Homography
 - 3x3 non singular matrix
 - there are 8 degrees of freedom
 - for calibration, we need minimum of ? points
 

### Calibration matrix K
```math
K = 
\begin{bmatrix}
f*k_u & 0 & u_0 \\
0 & f*k_v & v_0 \\
0 & 0 & 1
\end{bmatrix}
```
For the following example.
 - each pixel is 0.02mm * 0.02mm
 - focal length is 5mm
 - image principal point is at pixel (500, 500)
```math
K = 
\begin{bmatrix}
5*\frac{1}{0.02} & 0 & 500 \\
0 & 5*\frac{1}{0.02} & 500 \\
0 & 0 & 1
\end{bmatrix}
```


### Transformation from world to camera image
```math
P = K * [R t] = KR [I -C] 
```
 - C is camera position in the world reference frame
 - R is camera rotation matrix
 - t is camera translation vector

intrinsic parameters - matrix K
extrinsic parameters - matrix T

```math
\begin{bmatrix}
R & t \\
0 & 1
\end{bmatrix}
```

#### Extrinsic parameters (6)
 - 3 for rotation
 - 3 for translation


#### Intrinsic parameters (5)
 - focal length f
 - principle point u0, v0
 - pixel aspect ratio a (a is scalling along x axis)
 - skew angle phi, skew between the x and y axis

>  we have 11 degrees of freedom

#### Fundamental matrix 
In computer vision, the fundamental matrix {\displaystyle \mathbf {F} }\mathbf {F}  is a 3×3 matrix which relates corresponding points in stereo images. In epipolar geometry, with homogeneous image coordinates, x and x′, of corresponding points in a stereo image pair, Fx describes a line (an epipolar line) on which the corresponding point x′ on the other image must lie

essential matrix on [wiki](https://en.wikipedia.org/wiki/Essential_matrix)