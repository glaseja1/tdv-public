This file is related to the midterms.

# Midterm #1
This is the first midterm variant.

in `exams_other_school` you can find midterm tests from other faculties, found on various websites

Preparation on tests can be found in [preparation.md](./preparation.md)

Here is photo of the actual midterm test taken on FEL:

<img src="./img/test.jpg" width="40%">

    
### 1. (OK)
Is the point $`\underline{x} = (2,-2, 2)`$ (in homogeneous coordinates) incident with 
(located at) line $`\underline{n}=(1, 1, 1)`$
Prove your answer. (1 point)

<details>
  <summary>Click to expand!</summary>
  
  For incidency must apply
  ```math 
  \underline{x} . \underline{n} = \underline{x}^\top . \underline{n} = 0
  ```
  Where $`.`$ is the dot product
  ```math 
  \underline{x} . \underline{n} = 2 * 1 - 2 * 1 + 2 * 1 = 2
  ```
  > Therefore, the point does not lie on the line
  
  **Alternative middle school approach:**
  ```math 
  n: 1 * x + 1 * y + 1 = 0
  ```
  ```math 
  x = (1, -1)
  ```
  Plug it in:
  ```math 
  1 * 1 - 1 * 1 + 1 = 1
  ```
  > Therefore the point does not lie on the line
  
</details>
 
### 2. (OK)
>  TODO: solution not finished

We are given a homography
```math
H =
\begin{bmatrix}
h_{11} & h_{12} & h_{13} \\
h_{21} & h_{22} & h_{23} \\
h_{31} & h_{32} & 1
\end{bmatrix}
```
Express the homogeneous representation of a line n that is mapped by this homography to the line at infinity
noo. Is n necessarily a line at infinity (ideal line)? (1 point)

<details>
  <summary>Click to expand!</summary>
  
  The line at infinity is 
  ```math
  (0, 0, 1)
  ```
  
> H is always regular, as long as it is homography, therefore exists inversion.
  
  As we are projecting **line** to **line**, we must use $`H^\top`$
  ```math
  H^{-\top} * \underline{n} \cong \underline{n_{\infty}}=
  \begin{bmatrix}
0 \\
0 \\
1
\end{bmatrix}
  ```
Homography is regular matrix, so we can multiply by $`H^\top`$ from the left
```math
  H^{\top} * H^{-\top} * \underline{n} \cong =
H^{\top} *
\begin{bmatrix}
0 \\
0 \\
1
\end{bmatrix}
```

> it applies the following (according to slides):

```math
H^\top * H^{-\top} = 1
```

> this is because we can project using $`H^\top`$, and projection using $`H^{-\top}`$ is just the 
same projection, but on -1, so going back, like $`A * A^{-1} = 1`$ and $`A=H^\top`$

```math
\underline{n} =
\begin{bmatrix}
h_{31} \\
h_{32} \\
1
\end{bmatrix}
```

Can it be at infinity?

Yes, if $`h_{31} h_{32}`$ are both zero

</details>

### 3. (OK)
Show that matrix $`E \cong [t]_x`$, where $`t`$ is a real vector in R³ has one zero eigenvalue
and two non-zero eigenvalues of equal absolute value. (1 point)

<details>
  <summary>Click to expand!</summary>
  
  use defintion from slide **Cross Products and Maps by Skew-Symmetric 3 × 3 Matrices**
  
  ```math 
  |b|_x=
  \begin{bmatrix}
0 & -b_{3} & b_{2} \\
b_{3} & 0 & -b_{1} \\
-b_{2} & b_{1} & 0
\end{bmatrix}
  ```
  where 
  ```math 
  b = (b_1, b_2, b_3)
  ```
  The eigenvalues of $`[b]_x`$ are $`(0, \lambda, -\lambda)`$ (**that is from slide**)
  
  Therefore for $`[t]_x`$:
   ```math 
  |t|_x=
  \begin{bmatrix}
0 & -t_{3} & t_{2} \\
t_{3} & 0 & -t_{1} \\
-t_{2} & t_{1} & 0
\end{bmatrix}
  ```
  > There is a shortcut, as there is a definition:
  
  ```math
  \lambda(\lambda^2 + ||t||^2) = 0
  ```
  
  > or we can calculate classically
   
  To calculate eigenvalues, we calculate the roots of the characteristic equation
  ```math 
  det(A - \lambda I) = 0
  ```
  for $`[t]_x`$, the equation is 
  ```math 
  det(\begin{bmatrix}
0 & -t_{3} & t_{2} \\
t_{3} & 0 & -t_{1} \\
-t_{2} & t_{1} & 0
\end{bmatrix} - 
\begin{bmatrix}
 \lambda & 0 & 0 \\
0 & \lambda & 0 \\
0 & 0 & \lambda
\end{bmatrix} ) 
= 
det(\begin{bmatrix}
-\lambda & -t_{3} & t_{2} \\
t_{3} & -\lambda & -t_{1} \\
-t_{2} & t_{1} & -\lambda
\end{bmatrix} )
  ```
  
**Calculating the determinant of A**
    
```math
A = \begin{bmatrix}
-\lambda & -t_{3} & t_{2} \\
t_{3} & -\lambda & -t_{1} \\
-t_{2} & t_{1} & -\lambda
\end{bmatrix}
```
We use the **checkerboard rule** for which applies
```math
det(
\begin{bmatrix}
a & b & c \\
d & e & f \\
g & h & i
\end{bmatrix}
)
=
a * det(
\begin{bmatrix}
e & f \\
h & i
\end{bmatrix}
)
-
b * det(\begin{bmatrix}
d & f \\
g & i
\end{bmatrix}
)
+
c * det(\begin{bmatrix}
d & e \\
g & h
\end{bmatrix}
)
```
Therefore
```math
det(
\begin{bmatrix}
-\lambda & -t_{3} & t_{2} \\
t_{3} & -\lambda & -t_{1} \\
-t_{2} & t_{1} & -\lambda
\end{bmatrix}
)
=
-\lambda * det(
\begin{bmatrix}
-\lambda & -t_{1} \\
t_{1} & -\lambda
\end{bmatrix}
)
+t_{3} * det(\begin{bmatrix}
t_{3} & -t_{1} \\
-t_{2} & -\lambda
\end{bmatrix}
)
+
t_{2} * det(\begin{bmatrix}
t_{3} & -\lambda \\
-t_{2} & t_{1}
\end{bmatrix}
)
```
That is
```math 
-\lambda * (\lambda^2 - (-t_{1}^2))
+t_{3} * (-\lambda * t_{3} - t_{1} * t_{2})
+t_{2} * (t_{3}*t_{1} - \lambda * t_{2})
=
-\lambda^3 -\lambda * t_{1}^2
- \lambda t_{3}^2 - t_{1} * t_{2} * t_{3}
+t_{2} * t_{3}*t_{1} - \lambda * t_{2}^2
```
So the characteristic polynomial is:
```math
-\lambda^3 - \lambda t_1^2 - \lambda t_2^2 - \lambda t_3^2 = 0
```

  According to wolfram, then we get 
  ```math
  -\lambda^3 - \lambda t_1^2 - \lambda t_2^2 - \lambda t_3^2 = 0
  ```
  ```math
  -\lambda(\lambda^2 + t_1^2 + t_2^2 + t_3^2) = 0
  ```
  We get one result:
  ```math
  \lambda_0 = 0
  ```
  Continuing:
  ```math
  \lambda^2 + t_1^2 + t_2^2 + t_3^2 = 0
  ```
  ```math
  \lambda^2 = - t_1^2 - t_2^2 - t_3^2
  ```
  ```math
  \lambda_1 = \sqrt{- t_1^2 - t_2^2 - t_3^2}
  ```
  ```math
  \lambda_2 = -\sqrt{- t_1^2 - t_2^2 - t_3^2}
  ```
  > and that is the result, as 
  
  ```math
  |\lambda_1| = |\lambda_2|
  ```
  
</details>

### 4. (OK)
Let $`P = \begin{bmatrix}
Q & q
\end{bmatrix}`$ be the projection matrix of a perspective camera. What is the set X of 3D points that
project to image point $`\underline{m}`$ with homogeneous representation equivalent to q, i.e. 
$`m \cong q`$? (3 points)

<details>
  <summary>Click to expand!</summary>

Fom slides, we know that 
```math 
P = 
\begin{bmatrix}
Q & q
\end{bmatrix}
=
K * \begin{bmatrix}
R & t
\end{bmatrix}
=
\begin{bmatrix}
KR & Kt
\end{bmatrix}
```

  - the $`q`$ is the image of $`P\underline{x}`$
```math
P\underline{x} = \underline{m}
```
```math
P\underline{x} \cong q
```

> must be congruent, as $`\underline{m} \cong q`$

```math
P\underline{x} = Q\underline{x} + q
```

> no idea why, but seems same as for RT matrix, X * Rotation + translate

```math
Q\underline{x} + q \cong q
```

> we need to get rid of the congruation

```math
Q\underline{x} + q = \lambda q
```

I am looking for all $`x`$ that satisfy it

```math
x_1 = (0, 0, 0)
```

> this is the origin of the world coordinate system, so q is the image of the world coordinate system

$`\lambda`$ has to be the optical ray from the origin of the coordinate system to the centre of projecton

What wrote a friend to the test:

```math
Qx = 0
```
the vectors are null space of $`Q`$


</details>

### 5. (OK)
We have a single perspective image of a planar checkerboard pattern with square tiles of 
25 x 25 cm in size (see the picture). We know that the principal point of the camera is located a
and that the camera has square pixels in an orthogonal raster. We set up an orthogonal world 
coordinate system x – y – z (see the picture). What camera parameters can be determined from such a
picture? How do we obtain them (give the equations)? Someone told you that the rotation matrix R 
can be obtained by at least two different algorithms, can you confirm that (please explain)? (6 points)

<details>
  <summary>Click to expand!</summary>
  
  > usefull slide might be **Camera Orientation from Two Finite Vanishing Points**
  
  - we can determine **all** parameters
  - we can get the principle points, $`u_0`$ and $`v_0`$, by measuring the image, and dividing the **width** and **height** of the image by `2`
  - $`a=1`$ because **the camera has square pixels** in orthogonal raster
  - $`\Phi=90deg=\frac{\Pi}{2}`$ because the camera has square pixels in **orthogonal raster**
  - we can get focal length from 2 orhogonal vanishing points, they are both finite

  - we can get the rotation **R** from vanishing points (in image, in direction **x** and **y**)
    - this is not giving you the translation
  - note:
    - we have the origin of the coordinate systems (from question 4), so we have the last column of the projection matrix **up to scale**
        - so we know where is the camera on a line, but we do not kow the distance 
  - we can also use the **3 point calibration**, that gives **R** and **t**
    - we have calibration 
    - we have points (all in plane)
    - (0, 0)
    - (25, 0)
    - (0, 25)
  - you **never** use the camera resection 6 point algorithm here, because **all the points are in common plane**


</details>


# Midterm #2
This is the second midterm variant.

<img src="./img/test2_1.jpg" width="40%">
<img src="./img/test2_2.jpg" width="40%">

### 1. (OK)
Show algebraically that the following homography $`H`$ preserves the angle between any two finite lines $`\underline{n}`$ and $`\underline{n'}`$. (1 point)

> Hint: Be careful to work with a suitable representation of lines and a suitable formula for the angle.

<details>
  <summary>Click to expand!</summary>
  
  For angle, there is equation (from slides):
  ```math 
  cos(\Phi) \cong (H(\underline{x} - \underline{z}))^\top * (H(\underline{y} - \underline{z}))  
  ```
  So
  ```math 
  (H(\underline{x}) - H(\underline{z}))^\top * (H(\underline{y}) - H(\underline{z}))
  ```
  Knowing that:
   ```math 
  Hx = 
\begin{bmatrix}
R & t \\
0 & 1
\end{bmatrix}
*
\begin{bmatrix}
x \\
1
\end{bmatrix}
=
\begin{bmatrix}
Rx +t \\
1
\end{bmatrix}
  ```
  Then:
  ```math 
  (H(\underline{x}) - H(\underline{z}))^\top * (H(\underline{y}) - H(\underline{z})) 
  =
(\begin{bmatrix}
Rx + t \\
1
\end{bmatrix}
-
\begin{bmatrix}
Rz + t \\
1
\end{bmatrix})^\top 
* 
(\begin{bmatrix}
Ry + t \\
1
\end{bmatrix}
-
\begin{bmatrix}
Rz + t \\
1
\end{bmatrix})
  ```
  Which is 
  ```math 
(\begin{bmatrix}
Rx - Rz \\
0
\end{bmatrix})^\top 
* 
(\begin{bmatrix}
Ry - Rz \\
0
\end{bmatrix})
=
(Rx - Rz)^\top * (Ry - Rz) = (x - z) * R^\top R (y-z) = (x - z) * (y - z) \cong cos(\Phi)
  ```
  
  > Note: $`R^\top * R = I`$
  
  
</details>


### 2. 
We are given a point $`x`$ and a line $`n`$ (in their respective homogeneous representations). Point x is not incident with line n, i.e. nTx 0 0. Is there a homography H that maps x to x' and Li to Li so that g and n' become incident, i.e. (oT = 0? (1 point)

### 3. 
What class of homographies preserve (do not change) the canonical line at infinity = (0, 0,1) (they do not change the line but they may move the points on the line)? Derive the result by considering a general homography and showing that only some of them satisfy the requirement. (2 points)

### 4. (OK)
We are given two cameras P1 and P2. Are their optical axes parallel? Explain your answer. (1 point)

```math
P_1 = K_1 [R t_1]
```
```math
P_2 = K_2 [R t_2]
```

<details>
  <summary>Click to expand!</summary>
  We know that for optical axes applies:
```math 
o = det(Q) * q_3
```
  - $`o`$ is the optical axes line, in homogeneous coordinates

As we are interested only in orthogonality of the optical axes, we really do not care about $`det(Q)`$ as it is only a **constant**.


Also, the following applies for $`Q`$ and $`q`$:
```math 
P = 
\begin{bmatrix}
Q \\
q
\end{bmatrix}
=
K*
\begin{bmatrix}
R & t
\end{bmatrix}
=
\begin{bmatrix}
q_1^\top & q_{14} \\
q_2^\top & q_{24} \\
q_3^\top & q_{34}
\end{bmatrix}
```
> You can see, that $`q_3`$ is a vector, as a column

Let's now see, how we can get $`q_3`$.

```math
K * 
\begin{bmatrix}
R & t
\end{bmatrix}
=
\begin{bmatrix}
K_{11} & K_{12} & K_{13} \\
K_{21} & K_{22} & K_{23} \\
K_{31} & K_{32} & K_{33} 
\end{bmatrix}*
\begin{bmatrix}
R_{11} & R_{12} & R_{13} & t_{1} \\
R_{21} & R_{22} & R_{23} & t_{2} \\
R_{31} & R_{32} & R_{33} & t_{3} 
\end{bmatrix}
```

From that we can see, that 

```math
q_3^\top = (K_3 * R_{col1}, K_3 * R_{col2}, K_3 * R_{col3}) = K_3 * R
```

Where $`K_3`$ is the third row of the $`K`$ matrix

As the third row of the matrix $`K`$ is the same for both cameras, as well as the rotation $`R`$ is,
therefore:
```math
K_3^\top = 
\begin{bmatrix}
0 & 0 & 1
\end{bmatrix}
```

Answer: Yes, the optical axes are parallel

</details>

### 5.
There is a perspective camera mounted somewhere on our car, looking out. We have an image from the  
camera (below). By analyzing the image of the scene we were able to identify the vanishing line of the  
ground plane (the horizon; it is the solid red horizontal line). Can we guess the location of the camera in  
our car: Is it mounted in the front grille, located on the dashboard or mounted near the rearview mirror  
inside the car? Please explain your reasoning. (2 points)

### 6. (OK)
There is a set of 3D points X = n = 7 of known positions. All the points lie in the same plane. 
We observe an image of these points in a perspective camera with unknown projection matrix P. The
correspondences from X to the image are known. How can we estimate the parameters (resect) P from this information? (1 point)

<details>
  <summary>Click to expand!</summary>
  
  - we take 6 points, and use the **camera resection algorithm**
  - because all the 7 points lie in the same plane, we get non-unique solution
    - all the solutions lie on a line C (viz slides) 
  

</details>

### 7. (OK)
Is M a fundamental matrix? Why? (1 point)

```math
M =
\begin{bmatrix}
0 & 0 & 1 \\
0 & 0 & 0 \\
-1 & 0 & 0
\end{bmatrix}
```

<details>
  <summary>Click to expand!</summary>

For fundamental matrix 3x3 FFF, the following must apply:


 - is rank 2 (source: lecture 6, slide 5)
 - is skew symmetric (source: hint from homework)

So yes, M is a fundamental matrix

</details>


### 8. (OK)
A camera underwent a pure translation (without rotation). At the beginning the 
camera projection matrix was P1 = 0] and at the end of motion it was P2 = K[I, t21]. 
What is the fundamental matrix of the pair (P1. P2)? Simplify the result as much as 
possible, ideally your result will have the form [a], where a e 110, i.e. a 
skew-symmetric 3 x 3 matrix. Interpret the result as an image element. (1 point) 


<details>
  <summary>Click to expand!</summary>

So we are trying to get $`F`$.

It applies (from slide 77 of TDV)
```math
F=Q_2^{-\top}Q_1^{\top} [e_1]_x
= 
Q_2^{-\top}Q_1^{\top} * [-K_1 R_{21} t{21}]_x = ? = K_2^{-\top} [-t_{21}]_x R_{21}K_1^{-1}
```
 - we know $`K_1`$, $`K_2`$, $`R_{21}`$ and $`t_{21}`$

As also applies (source: slides)
 - $`R_{21} = R_2 R_1^\top`$ 
 - $`t_{21} = t_2 - R_21 t_1`$ 

</details>

### 9. (OK)
How would you generate a random projection matrix (from an arbitrary probability density)? (1 point)

Using matlab, the matrix can be generated as follows:

<details>
  <summary>Click to expand!</summary>
  
```py
Q = rand(3,3); 
while det(Q)==0, 
    Q = rand(3,3); 
end

P = [Q, rand(3,1)];
```

</details>