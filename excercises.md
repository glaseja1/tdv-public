# Excercises
Here are my own, personal excercises, fround generally around on web.

### Excercise 1
Being given camera matrix 
```math
C = 
\begin{bmatrix}
512 & -800 & 0 & 800 \\
512 & 0 & -800 & 1600 \\
1 & 0 & 0 & 0 
\end{bmatrix}
```
And a point in world coordinates
```math
p = (4, 0, 0)
```
Calculate the image coordinates of such a point.

#### Hint
if you do not know, how matrix C is formed, look [here](https://gitlab.fit.cvut.cz/glaseja1/doktorske-studium/blob/master/tdv/lectures.md#complete-transformation-overview)

#### Solution
<details>
  <summary>Click to expand!</summary>
  
You must convert the 3D point to homogenous coordinates first.
Then, you project the point by calculating

```math
C * \underline{x}^\top = 
\begin{bmatrix}
512 & -800 & 0 & 800 \\
512 & 0 & -800 & 1600 \\
1 & 0 & 0 & 0 
\end{bmatrix}
*
\begin{bmatrix}
4 \\
0 \\
0 \\
1
\end{bmatrix}
=
\begin{bmatrix}
512 * 4 + 800 & 512 * 4 + 1600 & 4
\end{bmatrix}
```

You **must** normalize the coordinates (convert from homogeneous to the cartesian)
```math
r 
=
\begin{bmatrix}
\frac{512 * 4 + 800}{4} & \frac{512 * 4 + 1600}{4} & \frac{4}{4}
\end{bmatrix}
```
 The image plane point is $`(712, 912)`$
</details>